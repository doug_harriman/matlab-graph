%EDGE  Edge class definition.
%

% <AUTHOR>
% </AUTHOR>

classdef (CaseInsensitiveProperties=true) edge < handle
%% Public Properties
    properties
        % Basic properties
        name     = '';
        weight   =  1;
        handle   = [];
        userdata = [];
    end
%% Public Methods
    methods
        %------------------------------------------------------------------
        % Node Constructor
        %------------------------------------------------------------------
        function obj = edge(varargin)
            if nargin > 0
                % Make sure we have even arg cnt
                if isodd(nargin)
                    error([toolbox_name ':' toolbox_module ':' 'badinput'],...
                        'Property/value pairs expected.');
                end
                
                % Set each property as requested
                props = properties(obj);
                for i = 1:2:nargin
                    % Input pair
                    prop = lower(varargin{i});
                    val  = varargin{i+1};
                    
                    % Check
                    if ~ismember(prop,props)
                        error([toolbox_name ':' toolbox_module ':' 'unknownproperty'],...
                            'Unknown property: %s',prop);
                    end
                    
                    % Set
                    obj.(prop) = val;
                end
            end
            
        end % constructor
        
        %------------------------------------------------------------------
        % Display
        %------------------------------------------------------------------
        function display(e)
            n_edge = length(e);
            if n_edge > 1
                disp(['Edge Array (' num2str(n_edge) ')']);
            end
            
            for i = 1:n_edge
                if isempty(e(i).name)
                    name = '(Unnamed)';
                else
                    name = e(i).name;
                end
                disp(['Edge: ' name ' [' num2str(e(i).weight) ']' ]);
            end
        end
        
        %------------------------------------------------------------------
        % Version
        % TODO: make private.
        %------------------------------------------------------------------
        function [toolbox_name,toolbox_module,toolbox_version] = version(obj)
            %<TOOLBOX_INFO>
            TOOLBOX_NAME    = 'GraphToolbox';
            TOOLBOX_MODULE  = 'Edge';
            TOOLBOX_VERSION = 1;
            %</TOOLBOX_INFO>

            if nargout == 0
                disp(' ')
                disp(['Toolbox: ' TOOLBOX_NAME]);
                disp(['Version: ' num2str(TOOLBOX_VERSION)]);
                disp(['Module : ' TOOLBOX_MODULE]);
                disp(' ')
                return;
            end

            toolbox_name    = TOOLBOX_NAME;
            toolbox_module  = TOOLBOX_MODULE;
            toolbox_version = TOOLBOX_VERSION;
        end % graph.version
    end % methods
    
end % classdef