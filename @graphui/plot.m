%PLOT  Plot a GraphUI object.
%

% TODO - Figure out axes creation behavior.
% TODO - Want to be able to store layout for replot, but don't want live
% link to graph objects.  Ie, don't want removal of a node to be live.
% TODO - Want to repect size of pre-existing axes objects.  This may affect
%        layout.

% <AUTHOR>
% </AUTHOR>

function [obj] = plot(obj)

% Update layout if needed
if obj.layoutdirty && strcmp(obj.layoutmode,'auto')
    obj = layout(obj);
end

% Enforce axes properties for GraphUI's
% REVISIT - Layout should probably have scaling based on current axes size
% if we're not creating a new axes object.
clf;
axes_handle = axes('Units','Inches',...
    'DataAspectRatio',[1 1 1],...
    'Box','On',...
    'NextPlot','Add',...
    'XTick',[],...
    'YTick',[]);

% Plot nodes
fcn = @(n) plot(n);
nodes     = cellfun(fcn,obj.nodes,'uniformoutput',false);
obj.nodes = nodes;

% Plot edges
arrow_on = false;
if obj.directed
    arrow_on = true;
end

fcn = @(e) plot(e,arrow_on);
edges     = cellfun(fcn,obj.edges,'uniformoutput',false);
obj.edges = edges;

% Provide space to edge of axes.
xlim = get(axes_handle,'XLim');
set(axes_handle,'XLim',[0 xlim(2)]+[-1 0]*range(xlim)*0.05);
set(axes_handle,'Units','Normalized');
title(strrep(obj.name,'_',' '));

% Store the object in the axes object.
setappdata(axes_handle,'graph',obj);

% If we have arrows, set a resize fcn so that arrowheads are updated on
% figure resize.
if arrow_on
    set(gcf,'ResizeFcn',@ArrowResize);
end

% Only output if requested.
if nargout < 1
    clear obj;
end


% Update arrow heads on figure resize.
function [] = ArrowResize(varargin)

arrow_han=findobj(get(gca,'children'),'type','patch');
    arrow(arrow_han);
