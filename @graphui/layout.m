%LAYOUT Generate a layout for a graph object.
% FILENAME=LAYOUT(GRAPH,PARAM_1,VALUE_1,...,PARAM_N,VALUE_N) writes
% generates positions for all graph objects for use with GRAPH/PLOT.
%
% Layout - Dot, Neato, Twopi.
%
% See also: @graph/plot.

% TODO - Update help text.
% TODO - Support font size default options.
% TODO - Support pre-specification of graph plot size.
% Use the dot graph property size to enforce on the generated graphics.
% Could be used in conjunction with an already existing axes object
% so that the axes obj size does not have to change.
% TODO - Convert to calling object version code.

% <USER_ID>
% </USER_ID>

function [gr] = layout(gr,varargin)

%<TOOLBOX_INFO>
TOOLBOX_NAME = 'GraphToolbox';
TOOLBOX_MODULE = 'GraphUI';
%</TOOLBOX_INFO>

debug_on = true;
debug_on = false;

% Error check inputs
if nargin < 1
    error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'wrongArgCount'],...
        'One input required.');
end

% Options
if gr.directed
    % DOT is default layout for directed graphs.
    default.layout = 'dot';
else
    % NEATO is default layout for undirected graphs.
    default.layout = 'neato';
end
opt = options(default,varargin{:});

% Error check options
layout_opts = {'dot','neato','twopi'};
if ~ismember(opt.layout,layout_opts)
    error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'wrongArgVal'],...
        'Layout must be one of: ''dot'', ''neato'' or ''twopi''.');
end

filename = tempname;
fid = fopen(filename,'w');
if (fid<0)
    error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'fileIOError'],...
        ['Unable to open temporary file: ' filename]);
end

% Header
fprintf(fid,'digraph VarGraph\n');
fprintf(fid,'{\n');

% Graph properties.

% Nodes
node_list = gr.nodehandles;
n_node = length(node_list);
for i = 1:n_node
    node_handle = node_list(i);
    node_name = getnode(gr,node_handle,'Name');
    if isempty(node_name)
        node_name = num2str(node_handle);
    end
    fprintf(fid,'\t%d [label="%s"];\n',node_handle,node_name);
end

% Default edge properties.
if strcmp(opt.layout,'neato')
    fprintf(fid,'\tedge [len=%f];\n',2);
end

% Edges
edge_list = gr.edgehandles;
n_edge = length(edge_list);
for i = 1:n_edge
    edge_han = edge_list(i);
    edge_name = getedge(gr,edge_han,'Name');
    [node_from,node_to] = nodes(gr,edge_han);
    
    if ~isempty(edge_name)
        fprintf(fid,'\t"%d" -> "%d" [comment="%d",arrowhead=none,label="%s"];\n',...
            node_from,node_to,edge_han,edge_name);
    else
        fprintf(fid,'\t"%d" -> "%d" [comment="%d",arrowhead=none,label=" "];\n',...
            node_from,node_to,edge_han);
    end
    
end

% Footer
fprintf(fid,'}\n');

% Close the output
fclose(fid);

% Run the layout program
format = '';

% With PC's, use the Mathworks version of DOT.
if ispc
    opt.layout = ['mw' opt.layout];
end
    
% end

[status,str] = system([opt.layout ' ' format ' ' filename]);
if status ~= 0
    error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'systemError'],...
        ['Unable to run external layout program: ' str]);
end

% Remove the temp file
if debug_on
    edit(filename)
    fn2 = tempname;
    fid = fopen(fn2,'w');
    fprintf(fid,'%s',str);
    fclose(fid);
    edit(fn2);
    delete(fn2);
end
delete(filename);

% Handle any line continuation characters
str = strrep(str,char([92 10]),'');
% idx = strfind(str,char([92 10]));
% for i = 1:length(idx)
%     str(idx(i):idx(i)+1) = [];
% end

% Parse the results
CR = char(10);
[line,str] = strtok(str,CR);
while ~isempty(line)
    % Break the line into the gne (grap/node/edge) designator and the
    % param section.
    [nge,line] = strtok(line,'[]');
    [param,line] = strtok(line,'[]');
    
    % Handle parameters based on NGE contents
    if ~isempty(findstr('digraph',nge))
        % Just header, skip.
    elseif ~isempty(findstr('node',nge))
        % Default node data, skip.
    elseif ~isempty(findstr('graph',nge))
        % Graph data
        param = Param2Struct(param);
        gr = ApplyGraphProps(gr,param);
        
    elseif ~isempty(findstr('-',nge))
        % Edge data
        node_han = regexp(nge,'\d+','match');
        node1 = str2num(node_han{1});
        node2 = str2num(node_han{2});
        param = Param2Struct(param);
        edge_han = str2num(param.comment);
        gr = ApplyEdgeProps(gr,edge_han,param);
        
        % If directed graph, extract edge to get edge direction correct.
        if gr.directed
            e = getedge(gr,edge_han);
            x = e.lineui.xdata;
            y = e.lineui.ydata;
            n2pos = getnode(gr,node2,'Position');
            d1 = sqrt((x(1) - n2pos(1))^2 + (y(1) - n2pos(2))^2);
            d2 = sqrt((x(end) - n2pos(1))^2 + (y(end) - n2pos(2))^2);
            
            if d2 > d1
                x = fliplr(x);
                y = fliplr(y);
                e.lineui.xdata = x;
                e.lineui.ydata = y;
                gr = setedge(gr,edge_han,e);
            end
        end
        
    elseif ~isempty(str2num(nge))
        % Node data
        node_han = str2num(nge);
        param = Param2Struct(param);
        gr = ApplyNodeProps(gr,node_han,param);
        
    else
        % Nothing recognized
    end
    
    % Read the next line.
    [line,str] = strtok(str,CR);
    line;
end

% Mark the layout as clean.
gr.layoutdirty = false;

% Update object if no output requested
if nargout == 0
    assignin('caller',inputname(1),gr);
end

%--------------------------------------------------------------------------
% ApplyGraphProps
% Apply DOT graph properties to graph object.
%--------------------------------------------------------------------------
function [gr] = ApplyGraphProps(gr,param)
return

%--------------------------------------------------------------------------
% ApplyEdgeProps
% Apply DOT node properties to edge object.
%--------------------------------------------------------------------------
function [gr] = ApplyEdgeProps(gr,edge_han,param)

if isempty(edge_han) || (edge_han==0)
    keyboard
end

% Convert DOT parameter values to node parameter values.
fields = fieldnames(param);
n_fields = length(fields);
for i = 1:n_fields
    field = fields{i};
    val = param.(field);
    switch(field)
        case 'arrowhead'
            % Skip
        case 'label'
            % Skip for now.
            % REVISIT - How to designate what to print with edges? Name,
            % weight, etc.
            
        case 'len'
            % Edge length from NEATO. Ignore.
            
        case 'pos'
            % Line points
            % REVISIT - swap line ends to get arrow head correct if
            % directed.
            val = str2num(val);
            pos = val/72; % Points to inches.
            x = pos(1:2:end);
            y = pos(2:2:end);
            gr = setedge(gr,edge_han,'XData',x);
            gr = setedge(gr,edge_han,'YData',y);
            
        case 'lp'
            % Label position.
            val = str2num(val);
            pos = val/72; % Points to inches.
            e = getedge(gr,edge_han);
            e.label_name.position = pos;
            gr = setedge(gr,edge_han,e);
            
        case 'comment'
            % Skip. Hiding edge handle in comment field to support
            % multiple edges.
        otherwise
            disp(['Unknown field: ' field]);
    end
end


%--------------------------------------------------------------------------
% ApplyNodeProps
% Apply DOT node properties to node object.
%--------------------------------------------------------------------------
function [gr] = ApplyNodeProps(gr,node_han,param)

% Convert DOT parameter values to node parameter values.
fields = fieldnames(param);
n_fields = length(fields);
for i = 1:n_fields
    field = fields{i};
    val = param.(field);
    switch(field)
        case 'label'
            % Skip, this came from the graph already.
        case 'pos'
            val = str2num(val);
            pos(1:2) = val/72; % Points to inches.
            gr = setnode(gr,node_han,'Position',pos);
        case 'width'
            val = str2num(val);
            gr = setnode(gr,node_han,'Width',val);
        case 'height'
            val = str2num(val);
            gr = setnode(gr,node_han,'Height',val);
        otherwise
            disp(['Unknown field: field']);
    end
end


%--------------------------------------------------------------------------
% Param2Struct
% Convert a DOT parameter string to a data structure with parameter values.
%--------------------------------------------------------------------------
function [s] = Param2Struct(str)

s = struct();
[param,str] = strtok(str,'=');
while ~isempty(str)
    % Drop the equal sign
    str = str(2:end);
    
    % If the next char in str is a '"', then need to eat all chars in the
    % quotes.
    if str(1) == '"'
        [val,str] = strtok(str,'""');
        str = str(2:end); % Drop the next comma
    else
        [val,str] = strtok(str,',');
    end
    str = str(2:end); % Drop the next space
    
    % Convert to struct element
    param(param==' ') = '';
    s.(param) = val;
    
    % Read next pair.
    [param,str] = strtok(str,'=');
end