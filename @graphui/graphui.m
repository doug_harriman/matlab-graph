%GRAPHUI  Graph user interface class definition.
%

% <AUTHOR>
% </AUTHOR>

% TODO - Overload addnode.  If passed a node obj, convert to nodeui.
% TODO - Should layout be graphui property, or only when plotting?

classdef (CaseInsensitiveProperties=true) graphui < graph

%% Properties    
    properties % Public get/set.
        layoutdirty   = true;  % Do we need to do a layout or not.
        layoutmode    = 'auto'; % Auto or manual.  Auto means relayout if dirty on plot.
    end

%% Public Methods    
    methods
        %------------------------------------------------------------------
        % GraphUI
        % Constructor
        %------------------------------------------------------------------
        function [obj] = graphui(varargin)
            
            % Promote Graph objects to GraphUI objects.
            if (nargin > 0) &&  isa(varargin{1},'graph')
                % Pull out graph
                g        = varargin{1};
                varargin = varargin(2:end);
                
                % Append current graph attributes to list to update.
%                 fields = fieldnames(struct(g));
                fields = properties(g);
                for i = 1:length(fields)
                    obj.(fields{i}) = g.(fields{i});
                end
               
                % Convert Node objects to NodeUI objects.
                for i = 1:length(obj.nodes)
                    obj.nodes{i} = nodeui(obj.nodes{i});
                end
                
                % Convert Edge objects to EdgeUI objects.
                for i = 1:length(obj.edges)
                    obj.edges{i} = edgeui(obj.edges{i});
                end
                
            end
                
            % Apply passed in property values.
            if length(varargin) > 1
                % Put the options into a struct.
                S = struct(obj);
                [S,changes,unhandled] = options(S,varargin{:});

                % Apply the changes.
                n_changes = length(changes);
                for i = 1:n_changes
                    change = changes{i};
                    obj.(change) = S.(change);
                end

                % Warn on the unhandled properties.
                n_unhandled = length(unhandled);
                [toolbox_name,toolbox_module] = version(obj);
                for i = 1:2:n_unhandled
                    prop = unhandled{i};
                    warning([toolbox_name ':' toolbox_module ':' 'unknownProperty'],...
                        'Unknown %s property (ignored): %s',class(obj),upper(prop));
                end
            end

        end % graphui constructor        
        
%% Property Set Functions
        %------------------------------------------------------------------
        % Set
        %------------------------------------------------------------------
        function obj = set.layoutmode(obj,value)
           legal_value = {'auto','manual'};
           value       = lower(value);
           if ~ismember(value,legal_value)
               [toolbox_name,toolbox_module] = version(obj);
               error([toolbox_name ':' toolbox_module ':' 'wrongArgValue'],...
                   'Property "layoutmode" must be either "auto" or "manual".');
           end
           
           obj.layoutmode = value;
        end
        
        % REVISIT - should not allow public set
        function obj = set.layoutdirty(obj,value)
           legal_value = [true false];
           if ~ismember(value,legal_value)
               [toolbox_name,toolbox_module] = version(obj);
               error([toolbox_name ':' toolbox_module ':' 'wrongArgValue'],...
                   'Property "layoutdirty" must be either true or false.');
           end
           
           obj.layoutdirty = value;
        end
        %------------------------------------------------------------------
        % Version
        % TODO: make private.
        %------------------------------------------------------------------
        function [toolbox_name,toolbox_module,toolbox_version] = version(obj)
            %<TOOLBOX_INFO>
            TOOLBOX_NAME    = 'GraphToolbox';
            TOOLBOX_MODULE  = 'GraphUI';
            TOOLBOX_VERSION = 1;
            %</TOOLBOX_INFO>

            if nargout == 0
                disp(' ')
                disp(['Toolbox: ' TOOLBOX_NAME]);
                disp(['Version: ' num2str(TOOLBOX_VERSION)]);
                disp(['Module : ' TOOLBOX_MODULE]);
                disp(' ')
                return;
            end

            toolbox_name    = TOOLBOX_NAME;
            toolbox_module  = TOOLBOX_MODULE;
            toolbox_version = TOOLBOX_VERSION;
        end % graph.version
        
    
        
        
    end % public methods
end % classdef

