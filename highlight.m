%HIGHLIGHT  Highlights nodes & edges in a plot of a graph.
%   HIGHLIGHT(GRAPH,HANDLES,COLOR) sets the display color for the nodes and edges
%   specified by HANDLES to the given COLOR.  COLOR is optional and is red
%   by default.  If GRAPH is not specified, the graph in the current axes
%   is assumed.
%
%   See also: graphui/plot.

% TODO - Update help text.
% TODO - Should be able to take subgraph and highlight it.
% TODO - Option to highlight complimentary nodes or edges given the other.
% TODO - Highlight for a given time
% TODO - Highlight with fade via timers.
% TODO - Other highlight options like edgewidth & line type.
% TODO - Highlight via diminishing non-highlighted objects (fade back).

% <AUTHOR>
% </AUTHOR>

function [] = highlight(g,han)

%<TOOLBOX_INFO>
TOOLBOX_NAME    = 'GraphToolbox';
TOOLBOX_MODULE  = 'GraphUI';
%</TOOLBOX_INFO>

% Error check inputs
if nargin < 1
    error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'wrongArgCount'],...
        'One input required.');
end
if nargin > 2
    error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'wrongArgCount'],...
        'Two inputs maximum.');
end

% If only 1 input, assume it is a handle list.
% Get graph from current axes.
if nargin < 2
    han = g;
    g = getappdata(gca,'graph');
end
if ~isappdata(gca,'graph')
    error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'wrongArgCount'],...
        'Current axes does not contain a graph plot.  A grap must be specified.');
end

% Now, the input must be handles.
if ~isnumeric(han)
    error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'wrongArgType'],...
        'Node and edge handles must be in a numeric array.');
end
    
% Cycle through paths automatically if given more than 1.
if size(han,2) > 1
    for i = 1:size(han,2)
        highlight(g,han(:,i));
    end
    return;
end

% Remove 0 handles, and split handles into nodes and edges.
han(han==0) = [];
han = reshape(han,numel(han),1);
node_han = intersect(han,g.nodehandles);
edge_han = intersect(han,g.edgehandles);

% Select.
setnode(g,node_han,'Selected',true);
setedge(g,edge_han,'Selected',true);

% Wait for user input.
disp('Press any key to remove highlight and continue');
pause;

% Deselect.
setnode(g,node_han,'Selected',false);
setedge(g,edge_han,'Selected',false);
