%GRAPHTESTCALLBACK  Node change callback test function.
%

function [gr] = graphtestcallback(gr,event,varargin)

switch(event)
    case 'addeventfcn'
        disp('Event handler added.');
    case 'rmeventfcn'
        disp('Event handler removed.');
    case 'addnode'
        disp('Node added.');
    case 'pre-rmnode'
        disp('Node about to be removed.');
    case 'post-rmnode'
        disp('Node removal complete.');
    case 'addedge',
        disp('Edge added.');
    case 'pre-rmedge',
        disp('Edge about to be removed.');
    case 'post-rmedge',
        disp('Edge removal complete.');
    case 'delete',
        disp('Graph being deleted.');
    otherwise
        disp(['Unknown message: ' event]);
end
        
