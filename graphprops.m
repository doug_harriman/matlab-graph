%GRAPHPROPS  Help and information about Graph object properties.
%  GRAPHPROPS(QUERY,PROPERTY)  returns the results of a QUERY about a given
%  Graph Object PROPERTY.
%
%  Supported queries:
%  Writable  - Returns true if the property can be written via a SET call.
%  PropNames - Returns list of properties names.
%  

% TODO - Add user help text for each property.
% TODO - Add user info about accessor fcns to use for non-writable props.

% <USER_ID>
% </USER_ID>

function [val] = graphprops(msg,prop)

%<TOOLBOX_INFO>
TOOLBOX_NAME    = 'GraphToolbox';
TOOLBOX_MODULE  = 'Graph';
TOOLBOX_VERSION = 1;
%</TOOLBOX_INFO>

% Output message to user if no inputs.
if nargin == 0,
    txt = {...
        'Name         - Graph name.'...
        '               Content: Single string.'...
        '               Additional accessors:'...
        '               >> char(graph_obj)'...
        ' '...
        'AdjMat       - Adjacency matrix.'...
        '               Content: Two dimensional double matrix.'...
        '               Additional accessors:'...
        '               >> mat = double(graph_obj)'...
        '               >> setedge(graph_obj,''weight'',weight_value)'...
        ' '...
        'Directed     - Directed edges.'...
        '               Content: logical.'...
        '               Setting from true to false may lead to information loss'...
        '               as the adjacency matrix and all edge data matrices are '...
        '               converted from full matrices to upper triangular matrices.'...
        ' '...
        'NodeNames    - Cell array of strings containing names of individual nodes.'...
        ' '...
        'EdgeNames    - Cell array of strings containing names of individual edges'...
        ' '...
        'EventFcn     - Array of function handles of registered graph event handlers.'...
        ' '...
        'NodeUserData - Cell array of data associated with individual nodes.'...
        ' '...
        'EdgeUserData - Cell array of data associated with individual edges.'...
        ' '...
        'UserData     - Additional information or data.'... 
        '               Content: Any MATLAB data type.'...
        ' '...
        'MetaData     - Additional information or data type stored in a structure.'...
        '               Content: Any MATLAB data type.'...
    };
    disp(char(txt));
    return;
end

% Object directly writable status.
writable.name          = true;
writable.directed      = true;
writable.handlecount   = false;
writable.nodehandles   = false;
writable.edgehandles   = false;
writable.nodenames     = false;
writable.edgenames     = false;
writable.eventfcn      = false;
writable.nodeuserdata  = false;
writable.edgeuserdata  = false;
writable.metadata      = true;
writable.userdata      = true;

switch lower(msg),
    case 'writable',
        if nargin > 1,
            val = writable.(prop);
        else,
            val = writable;
        end
    case 'propnames',
        val = fieldnames(struct(graph));
    otherwise
        error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'invalidQuery'],...
            ['Invalid graph object property query: ' msg]);
end
