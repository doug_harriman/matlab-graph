%DISPLAY  Edge object display.

% <USER_ID>
% </USER_ID>

function [] = display(e)

%<TOOLBOX_INFO>
TOOLBOX_NAME    = 'GraphToolbox';
TOOLBOX_MODULE  = 'Edge';
TOOLBOX_VERSION = 1;
%</TOOLBOX_INFO>

disp(' ');
n_edge = length(e);
if n_edge > 1
    disp(['Edge Array (' num2str(n_edge) ')']);
    for i = 1:length(e)
        disp(['Edge Name ' num2str(i) ': ' e(i).name]);
    end
else
    disp(['Edge Name: ' e.name]);
end
disp(' ');
