%PLOT Plot an edge.

function [e] = plot(e,arrow_on)

% Line
% Convert the points to a bezier curve.
e.lineui = e.lineui.bezier;
[han,e.lineui] = plot(e.lineui);

if nargin < 2
    arrow_on = false;
end
if arrow_on
    pt1 = [e.lineui.xdata(end-1),e.lineui.ydata(end-1)];
    pt2 = [e.lineui.xdata(end),e.lineui.ydata(end)];
    e.arrowhead = arrow(pt1,pt2);
end

% Label Handle
if ~isempty(e.handle)
    % Set some colors
    bg = get(gca,'color');
    e.label_handle.backgroundcolor = bg;
    e.label_name.backgroundcolor = bg;
    
    e.label_handle.string = ['(' num2str(e.handle) ')'];
    idx = round(length(e.lineui.xdata)/2);
    e.label_handle.position = [e.lineui.xdata(idx) e.lineui.ydata(idx)];
    [han,e.label_handle] = plot(e.label_handle);
end

% Label Name
[han,e.label_name] = plot(e.label_name);

if nargout < 1
    assignin('caller',inputname(1),e);
end

return;

% Text
name = e.name;
e.text_han = text(e.text.position(1),e.text.position(2),...
    name,...
    'Color',e.text.fontcolor,...
    'FontSize',e.text.fontsize,...
    'FontAngle',e.text.fontangle,...
    'FontName',e.text.fontname,...
    'FontWeight',e.text.fontweight,...
    'Interpreter',e.text.interpreter,...
    'VerticalAlignment','Middle',...
    'HorizontalAlignment','Center');

