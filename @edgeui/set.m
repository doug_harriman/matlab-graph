%SET  Set edge object properties.

% TODO - Update extract args so that it removes the extracted args from the
%        main list.  This makes the list get smaller with each extraction.
%        Can output a warning for any unhandled args.
% TODO - Edge currently only specifies some of the contained object
%        properties.  Are all relevant props covered, or should we add
%        more, or should we add to the struct if the user does a set with a
%        prop that we have not defaulted?

% <USER_ID>
% </USER_ID>

function [n] = set(n,varargin)

%<TOOLBOX_INFO>
TOOLBOX_NAME    = 'GraphToolbox';
TOOLBOX_MODULE  = 'Edge';
TOOLBOX_VERSION = 1;
%</TOOLBOX_INFO>

% Apply node top level options
[opt,changes,unhandled] = options(struct(n),varargin{:});

% Property specific actions
for i_change = 1:length(changes),
    change = changes{i_change};
    switch(change)
        % Combo
        case 'position'
            % Line

            % Text
            unhandled{end+1} = 'textposition';
            unhandled{end+1} = opt.position;
            

        case 'name'
            % Tell text obj to change string
            unhandled{end+1} = 'textstring';
            unhandled{end+1} = opt.(change);
            
        otherwise
    end

    % Apply the change
    n.(change) = opt.(change);
end

% Rectangle changes.
% Apply changes to owned objects.
objs = {'text','line'};
n_objs = length(objs);
for i = 1:n_objs
    prefix = objs{i};
    arg    = ExtractArgs(unhandled,prefix);
    n      = ApplyArg(n,arg,prefix);
end

% Set the class & assign the object.
if nargout == 0
    assignin('caller',inputname(1),n);
end

% Edge property set error checking
%         if length(han) > 1
%             % Multi value set.  
%             % User must provide either a single string or a cell array of
%             % strings.
%             switch(class(val))
%                 case 'char'
%                     if size(val,1) > 1
%                         error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'wrongArgSize'],...
%                             ['Number of indecies and values must be consistent.']);
%                     end
%                 case 'cell'
%                     if length(han) ~= length(val)
%                         error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'wrongArgSize'],...
%                             ['Number of indecies and values must be consistent.']);
%                     end
%                 otherwise 
%                     error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'wrongArgType'],...
%                         ['Multi edge name set requires either be a string or cell array of strings.']);
%             end
%         else
%             % Single value set.
%             % User must provide only a single string.
%             switch(class(val))
%                 case 'char'
%                     if size(val,1) > 1
%                         error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'wrongArgSize'],...
%                             ['Number of indecies and values must be consistent.']);
%                     end
%                 otherwise 
%                     error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'wrongArgType'],...
%                         ['Single edge name set requires a string.']);
%             end
%         end
%         
%         % Convert single char to cell array.
%         if ischar(val)
%             val = {val};
%         end
%         gr.edgenames(idx) = val;
% 
%     case 'weight'
%         % Error check multi-value set parameter sizes
%         if (length(han) ~= length(val)) && (length(val) ~= 1)
%             error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'wrongArgSize'],...
%                 ['Number of indecies and values must be consistent.']);
%         end
% 
% 
%     case 'userdata'
%         % Error check single & multi-value set parameter sizes
%         if length(han) > 1
%             % Multi value set.  
%             % User must provide either a single string or a cell array of
%             % strings.
%             switch(class(val))
%                 case 'cell'
%                     if (length(han) ~= length(val)) && (length(val) ~= 1)
%                         error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'wrongArgSize'],...
%                             ['Number of indecies and values must be consistent.']);
%                     end
%             end
%         end
%         
%         % Convert single data points to cell.
%         if ~iscell(val),
%             val = {val};
%         end
%         gr.edgeuserdata(idx) = val;
%         
%         
%     otherwise
%         error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'unknownProperty'],...
%             ['Unsupported node property: ' upper(prop)]);
% end
% 



%--------------------------------------------------------------------------
% ApplyArgs
% Applies arguments to a node owned handle graphics object.
%--------------------------------------------------------------------------
function [n] = ApplyArg(n,arg,prefix)

% Exit if not given any args
if isempty(arg)
    return;
end

% Apply to node copy of data so it is retained if replotted.
l_prefix = length(prefix);
n_arg    = length(arg);

% Remove prefix from argument parameter names.
v=ver('Matlab');
if ~strcmp(v.Version,'7.0.4')
    warning('Non-vectorized implementation for this version of Matlab');
    % REVISIT - Use anonymous fcn & cellfun to remove prefix from arg
    % names.
end
for i = 1:2:n_arg
    prop   = arg{i};
    prop   = prop(l_prefix+1:end);
    arg{i} = prop;
end

% Extract the object struct and apply the options to it.
obj_struct = n.(prefix);
arg_struct = cell2struct(arg(2:2:end),arg(1:2:end));
obj_struct = copyfields(arg_struct,obj_struct);  % Only copies what is already there.
n.(prefix) = obj_struct;

% Apply to GUI object
han = n.([prefix '_han']);
if ~isempty(han) && ishandle(han)
    % Loop on the arguments.
    % REVISIT - is there any way vectorize property set of handle graphics objs?
    for i = 1:2:n_arg
        % Extract the property/value pair
        prop = arg{i};
        val  = arg{i+1};

        % Apply it.
        set(han,prop,val);
    end
end

%--------------------------------------------------------------------------
% ExtractArgs
% Extract the arguments with the given prefix.
%--------------------------------------------------------------------------
function [arg_cell] = ExtractArgs(full_arg_cell,prefix)

% Look for prefix text.
props = lower(full_arg_cell(1:2:end));
idx = strfind(props,prefix);
idx = find(~cellfun('isempty',idx));
idx = (idx-1)*2 + 1;
idx = sort([idx idx+1]);
arg_cell = full_arg_cell(idx);


