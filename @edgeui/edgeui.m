%EDGEUI  EdgeUI class definition.
%

% <AUTHOR>
% </AUTHOR>

classdef (CaseInsensitiveProperties=true) edgeui < edge
%% Public Properties
    properties
        lineui       = lineui; % Line       
        label_name   = label;  % Main label text
        label_handle = label;  % Handle label.
        arrowhead    = [];     % Arrowhead.
        color        = [0 0 0];% Edge plotting color
        
        % Data to pass through to lineui.
        xdata = [];
        ydata = [];

        % Object selection
        selected          = false;
        selectedcolor     = 'r';
        selectedlinewidth = 2;
        
        % UI Controls
        buttondownfcn  = '';
        uicontextmenu  = [];

    end

%% Public Methods
    methods
        %------------------------------------------------------------------
        % edgeUI Constructor
        %------------------------------------------------------------------
        function obj = edgeui(varargin)
            
            % Promote Edge objects to EdgeUI objects.
            if (nargin > 0) &&  isa(varargin{1},'edge')
                % Pull out edge
                e        = varargin{1};
                varargin = varargin(2:end);
                
                % Append current graph attributes to list to update.
%                 fields = fieldnames(struct(e));
                fields = properties(e);
                for i = 1:length(fields)
                    obj.(fields{i}) = e.(fields{i});
                end
            end

            % Apply passed in property values.
            if length(varargin) > 1
                % Put the options into a struct.
                S = struct(obj);
                [S,changes,unhandled] = options(S,varargin{:});

                % Apply the changes.
                n_changes = length(changes);
                for i = 1:n_changes
                    change = changes{i};
                    obj.(change) = S.(change);
                end

                % Warn on the unhandled properties.
                n_unhandled = length(unhandled);
                [toolbox_name,toolbox_module] = version(obj);
                for i = 1:2:n_unhandled
                    prop = unhandled{i};
                    warning([toolbox_name ':' toolbox_module ':' 'unknownProperty'],...
                        'Unknown edge property (ignored): %s', upper(prop));
                end
            end

            % Default properties
            obj.label_handle.visible = 'off';
            obj.label_name.string = obj.name;
            obj.label_name.horizontalalignment = 'right';

            % Selection color
            obj.lineui.selectedcolor       = obj.selectedcolor;
            obj.lineui.selectedlinewidth   = obj.selectedlinewidth;
            obj.label_name.selectedcolor   = obj.selectedcolor;
            obj.label_handle.selectedcolor = obj.selectedcolor;

            % Userdata for self referencing.
            data.type                 = 'edge';
            data.handle               = obj.handle;
            obj.lineui.userdata       = data;
            obj.label_name.userdata   = data;
            obj.label_handle.userdata = data;
            
        end

        %------------------------------------------------------------------
        % Set Color
        %------------------------------------------------------------------
        function obj = set.color(obj,color)
            % Set color of all sub objects before returning.
            obj.label_handle.color = color;
            obj.label_name.color   = color;
            obj.lineui.color       = color;
            
            if ~isempty(obj.arrowhead) && ishandle(obj.arrowhead)
                set(obj.arrowhead,'FaceColor',color);
                set(obj.arrowhead,'EdgeColor',color);
            end
                
        end % set.color

        %------------------------------------------------------------------
        % Display
        %------------------------------------------------------------------
        % Use parent class display.
        
        %------------------------------------------------------------------
        % Version
        % TODO: make private.
        %------------------------------------------------------------------
        function [toolbox_name,toolbox_module,toolbox_version] = version(obj)
            %<TOOLBOX_INFO>
            TOOLBOX_NAME    = 'GraphToolbox';
            TOOLBOX_MODULE  = 'EdgeUI';
            TOOLBOX_VERSION = 1;
            %</TOOLBOX_INFO>

            if nargout == 0
                disp(' ')
                disp(['Toolbox: ' TOOLBOX_NAME]);
                disp(['Version: ' num2str(TOOLBOX_VERSION)]);
                disp(['Module : ' TOOLBOX_MODULE]);
                disp(' ')
                return;
            end

            toolbox_name    = TOOLBOX_NAME;
            toolbox_module  = TOOLBOX_MODULE;
            toolbox_version = TOOLBOX_VERSION;
        end % graph.version

        %------------------------------------------------------------------
        % Set/Get
        %------------------------------------------------------------------
        function [obj] = set.xdata(obj,value)
            obj.lineui.xdata = value;
        end % set.xdata
        
        function [obj] = set.ydata(obj,value)
            obj.lineui.ydata = value;
        end % set.xdata
    
        %------------------------------------------------------------------
        % Set Selected
        %------------------------------------------------------------------
        function obj = set.selected(obj,value)
            obj.selected              = value;
            obj.lineui.selected       = value;
            obj.label_name.selected   = value;
            obj.label_handle.selected = value;
            
            % Arrow heads don't have UI class
            if ishandle(obj.arrowhead)
                switch(value)
                    case true
                        set(obj.arrowhead,'FaceColor',obj.selectedcolor,...
                            'EdgeColor',obj.selectedcolor);
                    case false
                        set(obj.arrowhead,'FaceColor',obj.color,...
                            'EdgeColor',obj.color);
                    otherwise
                        error('Illegal selected state');
                end
            end
        end

        %------------------------------------------------------------------
        % Set SelectedColor
        %------------------------------------------------------------------
        function obj = set.selectedcolor(obj,value)
            obj.lineui.selectededgecolor   = value;
            obj.label_name.selectedcolor   = value;
            obj.label_handle.selectedcolor = value;
        end

        %------------------------------------------------------------------
        % Set SelectedLineWidth
        %------------------------------------------------------------------
        function obj = set.selectedlinewidth(obj,value)
            obj.selectedlinewidth        = value;
            obj.lineui.selectedlinewidth = value;
        end
        
        %------------------------------------------------------------------
        % Set ButtonDownFcn
        %------------------------------------------------------------------
        function obj = set.buttondownfcn(obj,value)
            obj.lineui.buttondownfcn       = value;
            obj.label_name.buttondownfcn   = value;
            obj.label_handle.buttondownfcn = value;
        end

        %------------------------------------------------------------------
        % Set UIContextMenu
        %------------------------------------------------------------------
        function obj = set.uicontextmenu(obj,value)
            obj.lineui.uicontextmenu       = value;
            obj.label_name.uicontextmenu   = value;
            obj.label_handle.uicontextmenu = value;
        end
        
        
    end % methods
    
end % classdef