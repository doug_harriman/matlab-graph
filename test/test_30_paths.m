%TEST_PATHS  Test of directed graph paths.

function [res] = test_paths

% Try passing in an undirected graph.
g = graph;
g.Directed=false;
caught_error = 0;
try
    p = dfs(g);
catch
    caught_error = 1;
end
if ~caught_error
    error('Should not calc paths for undirected graph.');
end

% Test of paths calculated.
g = graph;
addnode(g,5);
addedge(g,1,2);
addedge(g,2,3);
addedge(g,3,4);
addedge(g,1,4);
addedge(g,1,5);
addedge(g,5,3);

expected_paths = [     1     1     1
     6     9    10
     2     4     5
     7     0    11
     3     0     3
     8     0     8
     4     0     4];
p = dfs(g);

ind = p==expected_paths;
if ~all(all(ind))
    error('Path calculation error');
end
   
% Get unified call list
p = toposort(g);
expected_path = [1 2 5 3 4]';
ind = p==expected_path;
if ~all(all(ind))
    error('Path calculation error');
end

% Get cycles
new_e = addedge(g,4,5);
[p,c] = dfs(g);
c_exp = [     3     4     5
     8    12    11
     4     5     3
    12    11     8
     5     3     4
    11     8    12
     3     4     5];
if ~all(all(c==c_exp))
    error('Wrong cycle cacluated');
end
rmedge(g,new_e);

expected_paths = [5 11 3 8 4]';
p = dfs(g,5,4);
ind = p==expected_paths;
if ~all(all(ind))
    error('Path calculation error');
end

% Check path length
len=pathlen(g);
if ~all(len==[3 1 3])
    error('Path length calc failed.');
end

% Check edges
h = edges(g);
if ~all(isedge(g,h))
    error('Edge check failed.');
end

if isedge(g,-1)
    error('Graph says an edge with handle -1 exists, wrong!');
end

% Path lengths
p = dfs(g);
[l,mp]=max(g);
if l ~= 3
    error('Max path length error');
end
if ~all(all(p(:,[1 3])==mp))
    error('Max paths wrong');
end

[l,mp]=min(g);
if l ~= 1
    error('Max path length error');
end
if ~all(all(p(1:length(mp),2)==mp))
    error('Min path wrong');
end

% Cycle detection
g = graph;
addnode(g,4);
addedge(g,1,2);
addedge(g,2,3);
addedge(g,3,4);
addedge(g,4,2);
addedge(g,3,1);

% Do something illegal
caught_error = 0;
try 
    [p,c] = dfs(g);
catch
    caught_error = 1;
end
if ~caught_error
    error('Didn''t catch error on path gen of cyclic graph.');
end

% Get the paths and cycles.
[p,c] = dfs(g,1);
if ~isempty(p)
    error('Should have no non-cyclic paths.');
end

expect_path = [     1     2
     5     6
     2     3
     6     7
     3     4
     9     8
     1     2];
if ~all(all(c==expect_path))
    error('Error calculating cycle nodes');
end

% Test the quick check.
n = addnode(g);  % Needed as we currently can't do cycle detection w/o start node.
addedge(g,n,1);
[tf,c]=iscyclic(g);
if ~tf
    error('Missed cycles in graph.');
end
expect_path = [     1     2
     5     6
     2     3
     6     7
     3     4
     9     8
     1     2];
if ~all(all(c==expect_path))
    error('Error calculating cycle edges');
end



% Denote success
res = 1;