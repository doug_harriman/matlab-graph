%TEST_EDGE_CONNECTIONS

function [res] = test_edge_connections

% Create a graph and add some nodes
g = graph;
addnode(g,3);

% Fill in some edges
w1=10;n1='Edge 3/1';e1 = addedge(g,3,1,'Weight',w1,'Name',n1);
w2=20;n2='Edge 1/3';e2 = addedge(g,1,3,'Weight',w2,'Name',n2);
w3=30;n3='Edge 2/2';e3 = addedge(g,2,2,'Weight',w3,'Name',n3);

% Check list of edges
h = edges(g);
tf = ismember([e1 e2 e3],h);
if ~all(tf)
    error('Graph edges are wrong.');
end
w = getedge(g,h,'Weight');
tf = ismember([w1 w2 w3],w);
if ~all(tf)
    error('Graph weights are wrong.');
end
n = edgenames(g);
for i = 1:length(n)
    if ~strmatch(n(i),eval(['n' num2str(i)]),'exact')
        error('Graph names are wrong.');
    end
end

% Check which edges attach to each node
e4 = addedge(g,2,1,'Name','Edge 2/1');
[from,to] = edges(g,2);

% Expect 2 edges from node 2
if ~all(ismember([e3 e4],from))
    error('Did not find edges from node correctly.');
end

% Expect 1 edge to node 2
if to ~= e3
    error('Did not find edge to node correctly.');
end


res = 1;