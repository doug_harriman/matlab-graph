%TEST_SELF_LOOP

function [res] = test_self_loop

% Create a graph and add some nodes
g = graph;
addnode(g,5);

addedge(g,1,2);
addedge(g,2,3);
addedge(g,3,4);
addedge(g,1,4);
addedge(g,2,2);
addedge(g,5,5);

[e,n] = selfloop(g);

if ~all(ismember(e,[10 11]))
    error('Missed self loop edges.');
end

if ~all(ismember(n,[2 5]))
    error('Missed slef loop nodes.');
end

% Completed
res = 1;