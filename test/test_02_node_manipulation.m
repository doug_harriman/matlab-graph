%TEST_NODE_MANIPULATION

% TODO - Many node tests still missing.  See file.

function [res] = test_node_manipulation

% Create a graph and add some nodes
g = graph;
n_nodes = 20;
for i = 1:n_nodes,
    idx = addnode(g,'Name',['Node ' num2str(i)]);
    if idx ~= i
        error('Node index is wrong'); 
    end
end

% Try to add some nodes with illegal number of nodes
caught_error = 0;
try
    addnode(g,-10); 
catch
    caught_error = 1;
end
if ~caught_error
    error('Added negative number of nodes!');
end


caught_error = 0;
try, 
    addnode(g,0);   
catch
    caught_error = 1;
end
if ~caught_error
    error('Added zero nodes!');
end


caught_error = 0;
try, 
    addnode(g,Nan); 
catch
    caught_error = 1;
end
if ~caught_error
    error('Added NaN nodes!'); 
end


caught_error = 0;
try, 
    addnode(g,Inf); 
catch
    caught_error = 1;
end
if ~caught_error
    error('Added infinite nodes!'); 
end

% Add a basic node.
g = graph;
addnode(g);
if size(g) ~= 1, error('Simple add of single node failed.'); end

% Add nodes in a block.
g = graph;
addnode(g,10);
if size(g) ~= 10, error('Block add of nodes failed.'); end

% Test for existance stuff
g=graph;addnode(g,3);
addedge(g,1,3);addedge(g,1,2);addedge(g,2,3);addedge(g,3,1);
setnode(g,1,'Name','A');setnode(g,2,'Name','B');
if ~isnode(g,'A')
    error('ISNODE failure on single name lookup.');
end
if ~all(isnode(g,{'A','B'}))
    error('ISNODE failure on multiple name lookup.');
end
if isnode(g,'Blah')
    error('ISNODE failure on non-member node name lookup.');
end
if ~isnode(g,2)
    error('ISNODE failure on single node handle lookup.');
end
if ~all(isnode(g,[1 2 3]))
    error('ISNODE failure on multiple node handle lookup.');
end
if isnode(g,1234)
    error('ISNODE failure on non-member node handle lookup.');
end


% Remove some nodes.

% Set some node names

% Remove some more nodes.

% Add some back

% Check the node names.

res = 1;