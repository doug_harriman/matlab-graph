%TEST_NODE_MANIPULATION


function [res] = test_node_manipulation

% Create a graph and add some nodes
g = graph;
% addeventfcn(g,'graphtestcallback');
n1 = addnode(g,'Name','My Node 1');
n2 = addnode(g,'Name','My Node 2');
n3 = addnode(g,'Name','My Node 3');

% Add some edges
e1 = addedge(g,1,3,'Weight',7,'Name','My Edge 1');
e2 = addedge(g,2,1,'Weight',4,'Name','My Edge 2');
e3 = addedge(g,3,2,'Name','My Edge 3');  % Default

% Removed node 2.
% Should automatically delete edges 2 & 3
rmnode(g,n2);

% Test edge & node names
[n,e] = size(g);
if n ~= 2
    error('Wrong number of nodes');
end
if e ~= 1
    error('Wrong number of edges');
end
if ~strcmp('My Node 1',getnode(g,n1,'Name'))
    error('Node name wrong');
end
if ~strcmp('My Node 3',getnode(g,n3,'Name'))
    error('Node name wrong');
end
if ~strcmp('My Edge 1',getedge(g,e1,'Name'))
    error('Node name wrong');
end

% TODO - Mulitple node removal.
n4 = addnode(g);
n5 = addnode(g);
n6 = addnode(g);

% Add some edges
e4 = addedge(g,n4,n5);
e5 = addedge(g,n5,n4);
e6 = addedge(g,n6,n5);

% Removed node 2.
% Should automatically delete edges 2 & 3
rmnode(g,[n4 n5]);

% Another test
g = graph;
% addeventfcn(g,'graphtestcallback');
n1 = addnode(g,'Name','My Node 1');
n2 = addnode(g,'Name','My Node 2');
e1 = addedge(g,n1,n2,'Name','My Edge');

rmnode(g,n2);


res = 1;