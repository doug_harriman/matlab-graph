%TEST_GRAPH_CREATION

function [res] = test_graph_creation

% Create a graph, add some nodes and edges.
g = graph;

% Add nodes by several methods.
addnode(g);           % Unnamed node
addnode(g,'Name','My Node'); % Named node
addnode(g,10);        % Bunch of unnamed nodes.

% More nodes with names
n_nodes = 5;
for i = 1:n_nodes,
    addnode(g,'Name',['Node ' num2str(i)]);
end

% Add random edges
for i = 1:round(n_nodes/2),
    idx_from = round(rand*n_nodes+.5);
    idx_to   = round(rand*n_nodes+.5);
    addedge(g,idx_from,idx_to);
end


res = 1;

% Try to do some illegal things
% Add an edge with bad index types: negative, float, inf, nan.
% Add an edge to a non-existant node
% Add an edge that already exists.

