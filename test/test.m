%TEST  Graph toolbox unit test main function.

% TODO - Should provide an "error exected" fcn to test error handling.

function [] = test()

% Look for all files of name format: test_<specific test>.m
d = dir('test_*.m');

disp(' ');
disp('Running unit tests');

n_test = length(d);
for i_test = 1:n_test,
    [fpath,file] = fileparts(d(i_test).name);
    fprintf('\nRunning test: %s...',file);
    
    try,
       % Run the test file.  Can do something with the result.
       res(i_test) = feval(file);
       fprintf('succeeded');
    catch,
       % Call failed, so need to report it.
       res(i_test) = 0;
       fprintf('failed');
       fprintf('\nError: %s',lasterr);
    end
end

fprintf('\n');

if ~all(res),
    disp(['Test failed: ' num2str(length(res)-nnz(res)) '/' num2str(length(res))]);
else
    disp('Test succeeded');
end
