%TEST_EDGE_ADD_REMOVE

function [res] = test_edge_add_remove

% Create a graph and add some nodes
g = graph;
addnode(g,3);

% Fill in some edges
w1=10;n1='Edge 3/1';e1 = addedge(g,3,1,'Weight',w1,'Name',n1);
w2=20;n2='Edge 1/3';e2 = addedge(g,1,3,'Weight',w2,'Name',n2);
w3=30;n3='Edge 2/2';e3 = addedge(g,2,2,'Weight',w3,'Name',n3);

% Remove e2, and check values of others.
rmedge(g,e2);

check_n = getedge(g,e1,'Name');
if ~strcmp(check_n,n1)
    error('Edge 1 names do not match');
end

check_n = getedge(g,e3,'Name');
if ~strcmp(check_n,n3)
    error('Edge 3 names do not match');
end

check_w = getedge(g,e1,'Weight');
if check_w ~= w1
    error('Edge 1 weights do not match');
end

check_w = getedge(g,e3,'Weight');
if check_w ~= w3
    error('Edge 3 weights do not match');
end

% Remove the other two edges
rmedge(g,[e1 e3]);
[n,e] = size(g);
if e ~= 0
    error('Multi edge delete failed.');
end


% Fill in some edges
g = graph;
addnode(g,3);
n1='Edge 3/1';e1 = addedge(g,3,1,'Name',n1);
n2='Edge 1/3';e2 = addedge(g,1,3,'Name',n2);
n3='Edge 2/2';e3 = addedge(g,2,2,'Name',n3);

% Remove edges by names
rmedge(g,3,1);
[n_cnt,e_cnt] = size(g);
if e_cnt ~= 2
    error('Removal of edge by connected nodes failed.');
end

rmedge(g,[1 2],[3 2]);
[n_cnt,e_cnt] = size(g);
if e_cnt ~= 0
    error('Removal of multiple edges by connected nodes failed.');
end


res = 1;