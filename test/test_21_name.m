%TEST_NAME  Graph name testing

function [res] = test_name

g = graph;
if ~strcmp(g.Name,'Unnamed Graph')
    error('Wrong default graph name');
end

name = 'Test Name for Graph Obj';
g.name=name;
n1 = g.name;
n2 = char(g);
if ~strcmp(n1,n2),
    error('Graph names do not match');
end

res = 1;