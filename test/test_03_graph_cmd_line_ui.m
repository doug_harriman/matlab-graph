%TEST_GRAPH_CMD_LINE_UI

function [res] = test_graph_cmd_line_ui

% Create a graph and add some nodes, some named, some not.
g = graph;
h_n_un = addnode(g,2);  % 2 unnamed nodes
h_n_n1 = addnode(g,'Name','Named Node 1');
h_n_n2 = addnode(g,'Name','Named Node 2');

% Get handle of named node.
test_han = name2handle(g,'node','Named Node 2');
if test_han ~= h_n_n2,
    error('Node name lookup failed.');
end

% Add a couple of edges.
% 1 named, 1 unnamed.
h_e_1 = addedge(g,h_n_un(1),h_n_n2);
h_e_2 = addedge(g,h_n_un(2),h_n_n1,'Name','Named Edge 1');
% Overwrite the first edge.
h_e_3 = addedge(g,h_n_un(1),'Named Node 2','Name','Named Edge 2');

% Get handle of named node.
test_han = name2handle(g,'edge','Named Edge 1');
if test_han ~= h_e_2,
    error('Edge name lookup failed.');
end

% Display node & edge names
nodes(g)
edges(g)

res = 1;

