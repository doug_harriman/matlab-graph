%TEST_COMPONENTS  Tests for component finding


function [res] = test_35_comp

% Graph
g = graph;
addnode(g,8);

% First component
addedge(g,1,2);
addedge(g,2,3);
addedge(g,3,4);
addedge(g,1,4);
addedge(g,2,5);  % Pendand vertex

% Second component
addedge(g,6,7);

% Node 8 is unconnected
C = comp(g);
Cexp = [1 2 4 3 5; 6 7 0 0 0; 8 0 0 0 0]';
if ~all(all(C==Cexp))
    error('Component calculation error');
end

tf = isconn(g);
if tf
    error('ISCONN returned T when should be F');
end
    

% Success
res = 1;