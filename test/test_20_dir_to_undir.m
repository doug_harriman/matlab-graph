%TEST_DIR_TO_UNDIR  Tests conversion from directed to undirected graph.

function [res] = test_dir_to_undir

% Create a graph with different types of nodes.
g = graph('Name','Direction Conversion Graph');
g.directed=true;  % Default case, but do it anyway.

% Create 4 nodes
for i = 1:4,
    addnode(g,'Name',['Node ' num2str(i)]);
end

% Add a node that points to itself
addedge(g,1,1,'Name','Self pointer');

% Add nodes that form a cycle
addedge(g,2,3,'Name','Directed 2->3');
addedge(g,3,2,'Name','Directed 3->2');

% Add node on the lower triangle that will get moved
addedge(g,4,1,'Name','Lower Tri Node');

% Force to an undirected graph and back.
g.directed=false;
T = tril(adjmat(g),-1);
if any(any(T)), error('Directed to undirected graph conversion failure'); end

% TODO - Add test of node name change.

% Denote success
res = 1;