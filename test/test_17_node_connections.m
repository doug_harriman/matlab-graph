%TEST_NODE_CONNECTIONS

function [res] = test_node_connections

% Create a graph and add some nodes
g = graph;
nh = addnode(g,3);

% Fill in some edges
w1=10;n1='Edge 3/1';e1 = addedge(g,3,1,'Weight',w1,'Name',n1);
w2=20;n2='Edge 1/3';e2 = addedge(g,1,3,'Weight',w2,'Name',n2);
w3=30;n3='Edge 2/2';e3 = addedge(g,2,2,'Weight',w3,'Name',n3);

% Check list of nodes
[h,n] = nodes(g);
tf = ismember(h,nh);
if ~all(tf)
    error('Graph nodes are wrong.');
end

% Check which nodes attach to each edge
[from,to] = nodes(g,n1);

% Expect 2 edges from node 2
if from ~= 3
    error('Did not find starting node correctly.');
end

% Expect 1 edge to node 2
if to ~= 1
    error('Did not find ending node correctly.');
end


res = 1;