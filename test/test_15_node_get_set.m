%TEST_NODE_GET_SET


function [res] = test_node_get_set

% Create a graph and add some nodes
g = graph;
n1n = 'My Node 1';
n1 = addnode(g,'name',n1n);
n1ud = 12;
setnode(g,n1,'userdata',n1ud);

n2n = 'My Node 2';
n2 = addnode(g,'name',n2n);

n3n = 'My Node 3';
n3 = addnode(g,'name',n3n);

check_name = getnode(g,n1,'Name');
if ~strcmp(check_name,n1n),
    error('Node name check failed.');
end

check_name = getnode(g,n2,'Name');
if ~strcmp(check_name,n2n),
    error('Node name check failed.');
end

check_name = getnode(g,n3,'Name');
if ~strcmp(check_name,n3n),
    error('Node name check failed.');
end


rmnode(g,n2);
check_name = getnode(g,n1,'Name');
if ~strcmp(check_name,n1n),
    error('Node name check failed.');
end

caught_error = 0;
try
    check_name = getnode(g,n2,'Name');
catch
    caught_error = 1;
end
if ~caught_error        
    error('Able to retrieve name from deleted node.');
end

check_name = getnode(g,n3,'Name');
if ~strcmp(check_name,n3n),
    error('Node name check failed.');
end

check_ud = getnode(g,n1,'userdata');
if check_ud ~= n1ud 
    error('Userdata check failed.');
end

res = 1;