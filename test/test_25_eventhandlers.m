%TEST_EVENTHANDLERS  Test event handler functionality.

function [res] = test_eventhandlers

%<TOOLBOX_INFO>
TOOLBOX_NAME    = 'GraphToolbox';
TOOLBOX_MODULE  = 'Graph';
TOOLBOX_VERSION = 1;
%</TOOLBOX_INFO>

g = graph;
bad = @(x,y) (x+y)/(x-y);
addeventfcn(g,'graphtestcallback');

[nh] = addnode(g,2);
addedge(g,nh(1),nh(2));

if ~iseventfcn(g,'graphtestcallback')
    error('Didn''t recognize event function');
end

if iseventfcn(g,bad)
    error('False positive');
end

% Try to re-add same event fcn.
% Tests iseventfcn matching internal to addeventfcn.
lastwarn('');
warn = [TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'illegalValue'];
addeventfcn(g,'graphtestcallback');
[msg,id] = lastwarn;
if ~strmatch(warn,id,'exact')
    error('Allowed multiple sets of same fcn.');
end

% Remove the event handler
rmeventfcn(g,'graphtestcallback');

res = 1;