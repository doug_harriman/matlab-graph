%TEST_EDGE_MANIPULATION

% TODO - Many edge tests still missing.  See file.

function [res] = test_edge_manipulation

% Create a graph and add some nodes
g = graph;
addnode(g,3);

% Try to add some edges to illegal nodes.

% Add edges with various addedge syntaxes
e1w = 12;
e2w=  -6.5;
e1 = addedge(g,1,3,'Weight',e1w,'Name','Edge 1');

% Set an edge name.
new_name = 'Re-named Edge 1';
setedge(g,e1,'Name',new_name);
check_name = getedge(g,e1,'Name');
if ~strcmp(new_name,check_name),
    error(['Single edge name set failed.']);
end

% Add more edges
e2 = addedge(g,2,1,'Weight',e2w,'Name','Edge 2');
e3 = addedge(g,3,2,'Name','Default Edge');  % Default

% Set 2 edge names at the same time to the same value.
new_name = 'Same Edge Name';
setedge(g,[e1 e2],'Name',new_name);
check_name = getedge(g,e1,'Name');
if ~strcmp(new_name,check_name),
    error(['Multiple edges set to one name failed.']);
end
check_name = getedge(g,e2,'Name');
if ~strcmp(new_name,check_name),
    error(['Multiple edges set to one name failed.']);
end

% Set 2 edge names at the same time to different values.
warning('Temporarily disabled test');
% new_name = {'EDGE 1','EDGE 2'};
% setedge(g,[e1 e2],'Name',new_name);
% check_name = getedge(g,e1,'Name');
% if ~strcmp(new_name{1},check_name),
%     error(['Multiple edges set to multiple names failed.']);
% end
% check_name = getedge(g,e2,'Name');
% if ~strcmp(new_name{2},check_name),
%     error(['Multiple edges set to multiple names failed.']);
% end

% Try to set 3 edge names with 2 values
% caught_error = 0;
% try,
%     setedge(g,[e1 e2 e3],'Name',{'Name 1','Name 2'});
% catch
%     caught_error = 1;
% end
% if ~caught_error
%     error('Should not be able to set 3 egdes to 2 names.');
% end

% Edge weights
% Check the default edge weight.  Should be 1.
check_w = getedge(g,e3,'weight');
if check_w ~= 1
    error(['Incorrect edge weight returned (1)']);
end

% Check non-default weights 
check_w = getedge(g,e1,'weight');
if check_w ~= e1w
    error(['Incorrect edge weight returned (2)']);
end

% Change one
e1w = 125;
setedge(g,e1,'weight',e1w);
check_w = getedge(g,e1,'weight');
if check_w ~= e1w
    error(['Incorrect edge weight returned (3)']);
end

% Set 2 edge weights at the same time to the same value.
e1w = -1e6;
e2w = e1w;
setedge(g,[e1 e2],'weight',e1w);
check_w = getedge(g,e1,'weight');
if check_w ~= e1w
    error(['Incorrect edge weight returned (4)']);
end
check_w = getedge(g,e2,'weight');
if check_w ~= e2w
    error(['Incorrect edge weight returned (5)']);
end

% Throw in a new edge
e4 = addedge(g,3,3,'Name','Node 3 self ref');

% Set 2 edge weights at the same time to different values.
warning('Temporarily disabled test');
% e1w = 15;
% e2w = -3;
% setedge(g,[e1 e2],'weight',[e1w e2w]');
% check_w = getedge(g,e1,'weight');
% if check_w ~= e1w
%     error(['Incorrect edge weight returned (6)']);
% end
% check_w = getedge(g,e2,'weight');
% if check_w ~= e2w
%     error(['Incorrect edge weight returned (7)']);
% end
% han = [e1 e2];
% check_w = getedge(g,han,'weight');
% if ~all(ismember(check_w,[e1w e2w]))
%     error(['Incorrect edge weight returned (8)']);
% end


% % Try to set 3 edge weights with 2 values
% caught_error = 0;
% try,
%     setedge(g,[e1 e2 e3],'Weight',[11 13]);
% catch
%     caught_error = 1;
% end
% if ~caught_error
%     error('Should not be able to set 3 egdes to 2 names.');
% end


% Set an edge userdata.
ud1 = 'hi';
setedge(g,e1,'Userdata',ud1);
check_ud = getedge(g,e1,'UserData');
if ~strcmp(check_ud,ud1),
    error('Userdata incorrect');
end

% Set 2 edge userdata at the same time to the same value.
ud1 = 12;
setedge(g,[e1 e2],'UserData',ud1);
check_ud = getedge(g,e1,'UserData');
if check_ud ~= ud1
    error('Userdata incorrect');
end
check_ud = getedge(g,e2,'UserData');
if check_ud ~= ud1
    error('Userdata incorrect');
end

% Set 2 edge userdata at the same time to different values.
warning('Temporarily disabled test');
% ud1     = 1.5;
% ud2.str = 'hi';
% ud2.val = -1;
% setedge(g,[e1 e2],'UserData',{ud1 ud2});
% check_ud = getedge(g,e1,'UserData');
% if check_ud ~= ud1
%     error('Userdata incorrect');
% end
% check_ud = getedge(g,e2,'UserData');
% if ~strcmp(check_ud.str,ud2.str)
%     error('Userdata incorrect');
% end
% if check_ud.val ~= ud2.val
%     error('Userdata incorrect');
% end

% % Try to set 3 edge userdata's with 2 values
% caught_error = 0;
% try,
%     setedge(g,[e1 e2 e3],'UserData',{11 13});
% catch
%     caught_error = 1;
% end
% if ~caught_error
%     error('Should not be able to set 3 egdes to 2 userdata values.');
% end


% Set/get some properties
try,
    set(g,e1,'foo',12);
    error('Able to set illegal edge property "foo".');
end
try,
    set(g,[],'name','hi')
    error('Able to set name for edge with empty handle.');
end
try,
    set(g,-1,'name','hi')
    error('Able to set name for edge with handle = -1.');
end
try,
    set(g,NaN,'name','hi')
    error('Able to set name for edge with handle = NaN.');
end
try,
    set(g,Inf,'name','hi')
    error('Able to set name for edge with handle = Inf.');
end


res = 1;