%TEST_MULTIWEIGHT

function [res] = test_multiweight

g = graph('DefaultEdgeWeight',[0 0]);

addnode(g,5);

addedge(g,1,2,'weight',[1 2]);
addedge(g,2,3,'weight',[2 3]);
addedge(g,3,4,'weight',[3 4]);

addedge(g,1,5,'weight',[1 5]);
addedge(g,5,4,'weight',[5 4]);

l = pathlen(g);
if ~all(all(l==[6 6;9 9]))
    error('Path length calc wrong.');
end

% Now go to 4 weights.
g.defaultedgeweight = [0 0 0 0];
el = g.edges;
e = el{1};
if ~all(size(e.weight)==size(g.defaultedgeweight))
    error('Wrong weight size after default weight count increase');
end

% Now go to 3 weights.
g.defaultedgeweight = [0 0 0];
el = g.edges;
e  = el{1};
if ~all(size(e.weight)==size(g.defaultedgeweight))
    error('Wrong weight size after default weight count decrease');
end

% Try to set to wrong weight length.
caught_error = 0;
try 
    setedge(g,g.edgehandles(1),'weight',[1 2]);
catch
    caught_error = 1;
end
if ~caught_error
    error('Didn''t catch illegal weight size set.');
end

res = 1;