%TEST_TOPOSORT  tests topological sorting.


function [res] = test_36_toposort

% Graph
g = graph;
addnode(g,8);

% First component
addedge(g,1,2);
addedge(g,2,3);
addedge(g,3,4);
addedge(g,1,4);
addedge(g,2,5);  % Pendand vertex

% Second component
addedge(g,6,7);

% Get toposort
T = toposort(g);
Texp = [1 2 3 4 5; 6 7 0 0 0 ; 8 0 0 0 0 ]';
if ~all(all(T==Texp))
    error('Toposort error');
end

% Add a cycle and make sure we detect it.
addedge(g,4,2);
error_caught = 0;
try
    toposort(g);
catch
    error_caught = 1;
end
if ~error_caught
    error('Didn''t catch cyclic graph');
end

% Success
res = 1;