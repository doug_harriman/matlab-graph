%TEST_PROPERTIES  Tests GRAPHPROPS

function [res] = test_properties

% Try an unsupported query of a valid property
caught_error = 0;
try
    graphprops('foobar','AdjMat');
catch
   caught_error = 1; 
end    
if ~caught_error
    error('Able to do a non-existant query.');
end

% Try a supported query of an invalid property
caught_error = 0;
try,
    graphprops('writable','non-existant-property');
catch
   caught_error = 1; 
end    
if ~caught_error
    % That should not have worked.
    error('Able to query non-existant graph property');
end

% Make sure we get at least 1 valid query
val = graphprops('writable','directed');
if ~val,
    error('Wrong writable property returned for property directed.');
end

% Try to query names
graphprops('PropNames');

% Denote success 
res = 1;