%SHOWHANDLES  Shows node and edge handles on graph.
%

% TODO - Should support inputs like axes handle, 'edge' or 'node'. 
% TODO - Update error calls.
% TODO - Should use mapper (or iterator)

% <AUTHOR>
% </AUTHOR>

function [] = showhandles(msg)

% Error check inputs.
if nargin < 1
    msg = 'on';
end
msg = lower(msg);
if ~ismember(msg,{'on','off'})
    error('Valid inputs are ''on'' and ''off''.');
end

% Get graph from current axes.
% Loop on nodes & edges, turn on handle label.
axes_handle = gca;
if ~isappdata(axes_handle,'graph')
    error('Error: axes has no graph associated with it.');
end

% Turn on node handles
g = getappdata(axes_handle,'graph');
h = mapnode(g,@(n) n.label_handle.handle);
h = h(ishandle(h));
set(h,'Visible',msg);

% Set edge handle's background color to axes background color.
% c = get(axes_handle,'Color');

% Now turn on the text.
h = mapedge(g,@(e) e.label_handle.handle);
h = h(ishandle(h));
set(h,'Visible',msg);