%GRAPHS  List all graph objects in the Matlab base workspace.
%

% <AUTHOR>
% </AUTHOR>

function [] = graphs

%<TOOLBOX_INFO>
TOOLBOX_NAME    = 'GraphToolbox';
TOOLBOX_MODULE  = 'Graph';
%</TOOLBOX_INFO>

% Error check inputs
if nargin > 0
    error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'wrongArgCount'],...
        'No inputs expected');
end
if nargout > 0
    error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'wrongArgCount'],...
        'No outputs expected');
end

% Get all variables
vars = evalin('base','whos');

% Get var class and index.
fcn  = @(var) isa(evalin('base',var.name),'graph');
idx  = arrayfun(fcn,vars);
vars = vars(idx);

% Early exit if no graphs.
if isempty(vars)
    disp('No graphs in base workspace.');
    return;
end

% Setup string placeholders
var_str  = str2mat('Var Name','--------');
node_str = str2mat(' Nodes ',' ----- ');
edge_str = str2mat('Edges ','----- ');
name_str = str2mat('Graph Name',  '---------- ');

% Build string matrices by concatinating with var info.
for i = 1:length(vars)
    var = vars(i).name;
    [nodecnt,edgecnt] = evalin('base',['size(' var ');']);
    name = evalin('base',[var '.name;']);

    var_str  = str2mat(var_str,var);
    node_str = str2mat(node_str,[' ' int2str(nodecnt)]);
    edge_str = str2mat(edge_str,int2str(edgecnt));
    name_str = str2mat(name_str,name);
end

% Final display
disp(' ');
disp([var_str node_str edge_str name_str]);
disp(' ');
