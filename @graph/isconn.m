%ISCONN  Determines if a graph is connected.
%   [TF,COMPONENTS]=ISCONN(GRAPH) returns logical value TF depending on
%   whether GRAPH is connected or not.  Also returns a node component
%   matrix COMPONENTS, see graph/comp.
%
%   See also: graph, graph/comp.

% <USER_ID>
% </USER_ID>

function [tf,comp_mat] = isconn(gr)

%<TOOLBOX_INFO>
TOOLBOX_NAME    = 'GraphToolbox';
TOOLBOX_MODULE  = 'Graph';
%</TOOLBOX_INFO>

% Error check inputs
if nargin ~= 1
    error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'wrongArgCount'],...
        'One input expected.'); 
end

% Get the components.
comp_mat = comp(gr);
tf = size(comp_mat,2) == 1;