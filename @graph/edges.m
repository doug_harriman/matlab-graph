%EDGES  List edges in graph.
%   EDGES(GRAPH) displays a list of edges and the nodes connected by each
%   edge.
%
%   [HANDLES]=EDGES(GRAPH) returns a list of the edge HANDLES in the graph.
%
%   [EDGE_FROM,EDGE_TO]=EDGES(GRAPH,NODE) returns a list of handles for
%   the edges which emit FROM and connect TO the given NODE.  NODE can be
%   specified as a node name, as cell string array of names, a single node
%   handle or a vector of node handles.  If multiple nodes are specified,
%   the returned edge handles will be matrices with one row per node and zero
%   padded.  If there are no edges, a zero will be returned.
%
%   [HANDLE]=EDGES(GRAPH,NODE_FROM,NODE_TO) returns the HANDLEs of the
%   edges which connect NODE_FROM to NODE_TO.  NODE_FROM and NODE_TO may
%   each be either node names or node handles.
%
%   See also: graph/graph, graph/nodes, graph/addedge, graph/rmedge.
%

% <USER_ID>
% </USER_ID>

% To do:
% TODO - Build up output by cols so everything will line up.
% TODO - Update to use class version call.

function [varargout] = edges(gr,node,node2)

%<TOOLBOX_INFO>
TOOLBOX_NAME    = 'GraphToolbox';
TOOLBOX_MODULE  = 'Graph';
%</TOOLBOX_INFO>

% Determine which version of the function to run based on the input/output
% signature.
if (nargin==1) && (nargout == 0)
    % Simple display of graph nodes.
    EdgeDisplay(gr);
    
elseif (nargin==1) && (nargout > 0)
    % Return the handles for all edges in graph.
    han = unique(gr.edgehandles);
    han(han==0) = [];
    varargout{1} = han;
    
elseif (nargin==2) 
    % Edges which go to/from requested nodes.
    [from,to]    = EdgeConnections(gr,node);
    
    if isempty(to)
        to = [];
    end
    if isempty(from)
        from = [];
    end

    varargout{1} = from;
    varargout{2} = to;

elseif (nargin==3)
    % Two nodes provided, find the edge.
    [han] = FindEdge(gr,node,node2);
    varargout{1} = han;
    
else
    % Error
    error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'wrongArgCount'],...
        'Wrong number of inputs/outputs.  See "help edges" for more info.'); 
    
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Sub Functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%--------------------------------------------------------------------------
% EdgeConnections
% Return list of edge handles that go from and to the given nodes.
%--------------------------------------------------------------------------
function [from,to] = EdgeConnections(gr,node)

%<TOOLBOX_INFO>
TOOLBOX_NAME    = 'GraphToolbox';
TOOLBOX_MODULE  = 'Graph';
%</TOOLBOX_INFO>

% Node specification can be a single node or multiple nodes, and may be
% either names or handles.  Mixed names & handles not supported.
% Figure out what we have and convert to a vector of handles.
n_node = numel(node);
if isa(node,'char')
    % Single node name specified.
    node = name2handle(gr,'Node',node);
    
elseif isa(node,'cell')
    % List of names, convert to handles.
    handles = zeros(n_node,1);
    for i = 1:n_node
        % TODO - Use vectorized call of NAME2HANDLE when implemented.
        han = name2handle(gr,'Node',node(i));
        if isempty(han)
            error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'unknownNode'],...
                ['Unknown node name:' node(i)]);
        end
        handles(i) = han;
    end
    node = handles;
    
elseif isnumeric(node)
    % List of node handles, just validate.
    node = reshape(node,n_node,1);  % Row vector.
    ind  = ismember(node,gr.nodehandles);
    if ~all(ind)
        error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'unknownNode'],...
            'Illegal node handle specified.');
    end    
    
else
   % Unhandled data type 
    error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'wrongArgType'],...
        ['Unhandled data type for node specification: ' class(node)]); 
end

% Break down into lists of edge handles that go from the given node or to
% the given node.
if ~isempty(gr.edgelist)
    from = gr.edgehandles(ismember(gr.edgelist(:,1),node));
    to   = gr.edgehandles(ismember(gr.edgelist(:,2),node));
else
    from = [];
    to   = [];
end

%--------------------------------------------------------------------------
% EdgeDisplay
% Display list of edge connections for all edges in graph.
%--------------------------------------------------------------------------
function [] = EdgeDisplay(gr)

% Node separator depends on graph type.
if gr.directed
    nodesep = ' -> ';
else
    nodesep = ' <-> ';
end

disp(' ');
han_list = gr.edgehandles;
for i=1:length(gr.edgehandles)
    % Node names
    from_node = getnode(gr,gr.edgelist(i,1),'Name');
    to_node   = getnode(gr,gr.edgelist(i,2),'Name');

    name = getedge(gr,gr.edgehandles(i),'Name');
    if isempty(name),
        name = ['(Edge ' num2str(han_list(i)) ')'];
    end
    disp(['  ' name ': ' from_node nodesep to_node]);
end
disp(' ');


%--------------------------------------------------------------------------
% FindEdge
% Finds the edge which connects the two nodes.
%--------------------------------------------------------------------------
function [han] = FindEdge(gr,node_from,node_to)

% Convert from names to handles.
if isa(node_from,'char') || isa(node_from,'cell')
    node_from = name2handle(gr,'Node',node_from);
end
if isa(node_to,'char') || isa(node_to,'cell')
    node_to = name2handle(gr,'Node',node_to);
end

% Make sure we have row vectors.
node_from = reshape(node_from,length(node_from),1);
node_to   = reshape(node_to,  length(node_to),  1);

% Return the edge handle
tf  = ismember(gr.edgelist,[node_from node_to],'rows');
han = gr.edgehandles(tf);

% If have multiple edges to find, only return a row vector.
if size(han,1) > 1
    han = diag(han);
end
