%MAPNODE  Maps a function over all nodes in a graph.
%  RESULT=MAPNODE(GRAPH,FCN_HAN) maps the FCN_HAN over all of the
%  nodes of the GRAPH returning result.
%
%  RESULT=MAPNODE(GRAPH,FCN_HAN,'UniformOutput',false) removes the uniform
%  output restriction.  See CELLFUN.
%
% >> map(g,@fcn)
% fcn(graph,node)
%
% See also: graph, mapedge, cellfun.

% <AUTHOR>
% </AUTHOR>

% TODO - Provide passthrough for UniformOutput.
% TODO - Provide method for allowing fcn with no output.
% TODO - Update help text.

function [res] = mapnode(obj,fcn,varargin)

% Error check inputs
[toolbox_name,toolbox_module] = version(obj);
if nargin < 2
    error([toolbox_name  ':' toolbox_module ':' 'wrongArgCount'],...
        'Two inputs required.'); 
end

% Default options.
opt.uniformoutput = true;
opt = options(opt,varargin{:});

% Apply function
res = cellfun(fcn,obj.nodes,'UniformOutput',opt.uniformoutput);