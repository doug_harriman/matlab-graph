%PATHLEN  Caculates the edge weights along the specified paths in a graph.
%   [LENGTH,PATH]=PATHLEN(GRAPH,PATH) calculate the LENGTH of the PATHs in 
%   GRAPH.  LENGTH is defined to be the sum of the edge weights along each
%   PATH where the weights are determined by graph/pathweight.  LENGTH is a
%   matrix of size num_weights by num_paths where num_weights is the number
%   of weights that each edge has.
%
%   [LENGTH,PATH]=PATHLEN(GRAPH) uses graph/dfs with default options to 
%   generate the paths. 
%
%   See also: graph, graph/dfs, graph/pathweight.

% <USER_ID>
% </USER_ID>

function [w,out_path] = pathlen(gr,path)

%<TOOLBOX_INFO>
TOOLBOX_NAME    = 'GraphToolbox';
TOOLBOX_MODULE  = 'Graph';
%</TOOLBOX_INFO>

% Error check inputs
if (nargin > 2)
    error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'wrongArgCount'],...
        'Two inputs maximum.');
end

% Default path
if nargin < 2
    path = [];
end

% Get the weights & sum up
[w,out_path] = pathweight(gr,path);
w = sum(w);
w = squeeze(w);

% If have multiple weights, need to transpose.
if length(gr.defaultedgeweight) > 1
    w=w';
end
