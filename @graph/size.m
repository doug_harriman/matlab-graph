%SIZE  Graph size.
%   [NODE_COUNT,EDGE_COUNT]=SIZE(GRAPH)
%   [[NODE_COUNT EDGE_COUNT]]=SIZE(GRAPH)
%   SIZE(GRAPH)  Size output to Matlab command window.
%

% <USER_ID>
% </USER_ID>

% TODO - Update error() syntax to pass error types.  See addnode.m for example.

function [varargout] = size(gr)

%<TOOLBOX_INFO>
TOOLBOX_NAME    = 'GraphToolbox';
TOOLBOX_MODULE  = 'Graph';
TOOLBOX_VERSION = 1;
%</TOOLBOX_INFO>

% Error check inputs.
if nargin ~= 1, error('One input expected.'); end

% Number of nodes and edges in graph.
node_cnt = length(gr.nodehandles);
edge_cnt = length(gr.edges);

% Package output.
switch nargout,
    case 0,
        disp(['Node Count: ' num2str(node_cnt)]);
        disp(['Edge Count: ' num2str(edge_cnt)]);
    case 1,
        varargout{1} = [node_cnt edge_cnt];
    case 2,
        varargout{1} = node_cnt;
        varargout{2} = edge_cnt;
    otherwise,
        error('Maximum of 2 outputs.');
end
