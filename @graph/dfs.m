%DFS  Depth first search of a graph.
%  [PATH,CYCLE_PATH]=DFS(GRAPH)
%  [PATH,CYCLE_PATH]=DFS(GRAPH,START_NODE)
%  [PATH,CYCLE_PATH]=DFS(GRAPH,START_NODE,END_NODE) generates a 
%  PATH matrix with column vectors of node and edge handles of all paths from 
%  START_NODE to the END_NODE for the graph object GRAPH.  
%
%  Vectors are zero padded as needed.  START_NODE and STOP_NODE may both be
%  specified as node handles or node names and may be scalar or vectors.
%
%  CYCLE_PATH will be empty for acyclic graphs.  If non-empty, CYCLE_PATH
%  is path matrix like PATH denoting the nodes and edges in any cycles.
%
%  START_NODE and STOP_NODE are optional.  If ommitted, START_NODE defaults
%  to the start node(s) of GRAPH.  See graph/start for more information.
%  STOP_NODE defaults to the stop node(s) of the graph.  See graph/stop
%  for more information.  If the graph is cyclic and no start nodes exist, 
%  then START_NODE must be specified. 
%
%  See also: graph, graph/start, graph/stop.

% TODO - Results are non-intuitive if the graph has a start node but no
% stop node, and thus contains cycles.  Should give a warning.
% TODO - Add help text and example for calulating path weights
% TODO - Complete help text.

% <USER_ID>
% </USER_ID>

function [path_node,cycle_node] = dfs(gr,start_node,stop_node)

%<TOOLBOX_INFO>
TOOLBOX_NAME    = 'GraphToolbox';
TOOLBOX_MODULE  = 'Graph';
%</TOOLBOX_INFO>

% Error check inputs
if (nargin < 1)
    error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'wrongArgCount'],...
        'One input required.');
end
if (nargin > 3)
    error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'wrongArgCount'],...
        'Three inputs maximum.');
end
if (nargin>1) && any(~isnode(gr,start_node))
    error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'wrongArgType'],...
        'Start node incorrectly specified.  See: >> help graph/dfs');
end
if (nargin > 2) && any(~isnode(gr,stop_node))
    error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'wrongArgType'],...
        'Stop node incorrectly specified');
end
if ~gr.directed
    error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'graphNotDirected'],...
        'PATHS only supported for directed graphs.');
end

% Default inputs
if nargin < 2
    start_node = start(gr);
    if isempty(start_node)
        error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'cyclicGraph'],...
            ['Graph is cyclic with no start nodes.' char(10) ...
             'A start node must be specified to generate a depth first search of this graph.']);
    end
end
if nargin < 3
    stop_node = 0;
end

% Convert node names to handles if needed.
if ~isnumeric(start_node)
    start_node = name2handle(gr,'node',start_node);
end
if ~isnumeric(stop_node)
    stop_node = name2handle(gr,'node',stop_node);
end

% Depth first search of all paths.
path_node = reshape(start_node,1,length(start_node));
cur_path_idx = 1;
while cur_path_idx <= size(path_node,2)
    cur_path     = path_node(:,cur_path_idx);
    cur_path     = cur_path(cur_path~=0);
    cur_node_idx = length(cur_path);
    cur_node     = cur_path(cur_node_idx);

    % Cycle nodes are nodes that occur twice in the list.
    cycle_node  = intersect(cur_path(1:end-1),cur_node);

    % If we've reached a stop node, we're done with this path.
    if ~isempty(cycle_node)
        % If the current node (cycle_node) is already in the list, we have
        % a cycle here.  Need to tack it on, but with a negated node
        % handle to note the cycle.  
        path_node(cur_node_idx,cur_path_idx) = -cur_node;

        % Want cyclic path to be only the cycle involved.  Must remove all
        % nodes that exist before the cycle.
        idx = find(cur_path==cycle_node);
        temp_path = path_node(idx(1):idx(2),cur_path_idx);
        n_rows = size(path_node,1);
        temp_path = [temp_path; zeros(n_rows-length(temp_path),1)];
        path_node(:,cur_path_idx) = temp_path;
        
        % Move on to next path
        cur_path_idx = cur_path_idx + 1;

    elseif any(cur_node==stop_node)
        cur_path_idx = cur_path_idx + 1;
        
    else
        % Find the next nodes in the chains.
        % Cases:
        % Next node = 0 -> no more nodes in this chain.
        % Next node = vector -> extend the path_node
        next_node = NextNode(gr,cur_node);
        if next_node == 0
            cur_path_idx = cur_path_idx + 1;
        else
            % Tack on the first "next node" in place.
            path_node(cur_node_idx+[1 2],cur_path_idx) = next_node(:,1);

            % Build width chains from this node.
            new_chains = cur_path * ones(1,size(next_node,2)-1);

            % Tack on the other nodes for that path.
            new_chains = [new_chains; next_node(:,2:end)];

            % Zero pad if needed.
            pad_len = size(path_node,1) - size(new_chains,1);
            if pad_len > 0
                new_chains = [new_chains; zeros(pad_len,size(next_node,2)-1)];
            end

            % Add the new chains into the path matrix
            path_node = [path_node new_chains];
        end
    end
end

% If we were given a stop node, then remove all paths that don't end on it.
if any(stop_node)
    % Extract just the paths that actually end with the stop nodes.
    idx = ismember(path_node,stop_node);
    idx = any(idx);
    path_node = path_node(:,idx);

    % Drop any all zero rows
    if size(path_node,2) > 1
        idx = ~any(path_node')';
        path_node(idx,:) = [];
    else
        path_node(path_node==0) = [];
    end
end

% Sort the paths so the like paths are grouped together
path_node = sortrows(path_node')';

% Separate out cycles.
cycle_idx = any(path_node<0);
if any(cycle_idx)
    % Extract cycles.
    cycle_node = path_node(:,cycle_idx);
    
    % Eliminate cycles from main paths.
    path_node = path_node(:,~cycle_idx);
    if isempty(path_node)
        path_node = [];
    end
    
    % Elimintate negative node handles from cycles, and clean up extra
    % rows.
    cycle_node = abs(cycle_node);
    
    % Drop any all zero rows
    if size(cycle_node,2) > 1
        idx = ~any(cycle_node')';
        cycle_node(idx,:) = [];
    else
        cycle_node(cycle_node==0) = [];
    end
    
    % Make sure cycles are unique.
    % Two cases can occur, completely the same path, or paths that start in
    % different portions of the cycle, but are really the same cycle.  The
    % first case is easier to vectorize, so do it first to optimize run
    % time.
    % First case.
    cycle_node = unique(cycle_node','rows')';
    
    % Second case.
    cur_col = 2;
    while cur_col <= size(cycle_node,2)
        idx = zeros(1,size(cycle_node,2));
        idx(cur_col) = 1;
        if isempty(setdiff(cycle_node(:,cur_col)',cycle_node(:,1:cur_col-1)','rows'))
            cycle_node = cycle_node(:,~idx);
        else
            cur_col = cur_col + 1;
        end
    end
    
end

% Make sure all paths are unique
path_node = unique(path_node','rows')';


%--------------------------------------------------------------------------
% NextNode
% Give a node, finds all of the nodes which occur next.
%--------------------------------------------------------------------------
function [next_node,from] = NextNode(gr,cur_node)

from = edges(gr,cur_node);
if isempty(from)
    next_node = 0;
    return;
end
[temp,next_node] = nodes(gr,from);

next_node = [from; next_node'];
