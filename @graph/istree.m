%ISTREE  Determine if a graph is a tree.
%  TF=ISTREE(GRAPH) Returns logical value TF depending on whether GRAPH is
%  a tree or not.  A graph is determined to be a tree if it is connected,
%  directed, acyclic and all nodes have at most one edge entering.
%
%  See also: graph/isconn, graph/iscyclic.
%

function tf = istree(gr)

%<TOOLBOX_INFO>
TOOLBOX_NAME    = 'GraphToolbox';
TOOLBOX_MODULE  = 'Graph';
%</TOOLBOX_INFO>

% Error check inputs
if nargin ~= 1
    error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'wrongArgCount'],...
        'One input expected.'); 
end

% Assume false
tf = false;

% Check connectivity
if ~isconn(gr)
    return;
end

% Check directed
if ~gr.directed
    return;
end

% Check acyclic
if iscyclic(gr)
    return;
end

% Check the edge incidence of each node.
inc = deg(gr);

% Any number of outgoing edges allowed, check only incoming.
inc = inc(:,2);
if any(inc>1)
    % Have more than one incoming edge.
    return;
end

% Success, have a tree.
tf = true;
