%ISEVENTFCN  Returns true if the given function is an event handler for graph.
%   [TF]=ISEVENTFCN(GRAPH,FCN)  Check whether FCN is already and event
%   handler function for graph.
%
% See also: graph, addeventfcn, rmeventfcn.
%

% <USER_ID>
% </USER_ID>

function [tf] = iseventfcn(gr,fcn)

%<TOOLBOX_INFO>
TOOLBOX_NAME    = 'GraphToolbox';
TOOLBOX_MODULE  = 'Graph';
TOOLBOX_VERSION = 1;
%</TOOLBOX_INFO>

% Error check inputs
if nargin ~= 2
    error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'wrongArgCount'],...
        ['Two inputs required.']); 
end

% Convert function names to handles
if ischar(fcn)
    existval = exist(fcn);
    if ~any(existval == [2 3 5 6])
        error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'wrongArgType'],...
            ['String value is not function: ' fcn]);
    end
    
    fcn = str2func(fcn);
end

% Check function handle
if ~isa(fcn,'function_handle')
    error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'wrongArgType'],...
        ['Input is not a valid function handle.']); 
end

% Get full path to function & check against that for each.
test_fcn = functions(fcn);
test_fcn = test_fcn.file;

% Don't allow anonymous functions
if isempty(test_fcn)
    error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'wrongArgType'],...
        ['Can not test against anonymous functions.']); 
end

tf = logical(0);
n_fcn = length(gr.eventfcn);
for i = 1:n_fcn
    fcn = functions(gr.eventfcn{i});
    fcn = fcn.file;
    
    if strmatch(fcn,test_fcn,'exact')
        tf = logical(1);
        return;
    end
end


