%ISEDGE  Test whether an edge exists in a graph.
%   TF=ISEDGE(GRAPH,HANDLE)
%   TF=ISEDGE(GRAPH,NAME) both return true if the given edge HANDLE or edge
%   NAME exist as an edge in the GRAPH object.  HANDLE may be a vector of
%   handles and NAME may be a cell string array of names.
%
%   See also: graph, graph/addedge, graph/handle2index, graph/isnode.
%

% <USER_ID>
% </USER_ID>

function [tf] = isedge(gr,id)

%<TOOLBOX_INFO>
TOOLBOX_NAME    = 'GraphToolbox';
TOOLBOX_MODULE  = 'Graph';
TOOLBOX_VERSION = 1;
%</TOOLBOX_INFO>

% Error check inputs
if nargin ~= 2,
    error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'wrongArgCount'],...
        ['Two inputs required.']); 
end

% Handle any numeric type
if isnumeric(id)
    id = double(id);
end

% Switch on input type
switch(class(id))
    case {'char','cell'}
        % Single node name input.
        [tf] = ismember(id,gr.edgenames);

    case 'double'
        % Single or list of node handles.
        [tf] = ismember(id,gr.edgehandles);

        % Allow 0 as a passthrough.
        tfz = ismember(id,0);
        tf = tf | tfz;
        
    otherwise
        error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'wrongArgType'],...
            ['Unsupported edge specification type: ' class(id)]);
end