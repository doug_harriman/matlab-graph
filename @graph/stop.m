%STOP  List of nodes which terminate a directed graph.
%   [NODE_HAN]=STOP(GRAPH) returns a NODE_HAN which have an exit degree of
%   zero and a non-zero enter degree.  These are the nodes which "stop" the
%   directed graph.
%
%   See also: graph/deg, graph/start.

% <AUTHOR>
% </AUTHOR>

function [han] = stop(gr)

% Error check inputs
[toolbox_name,toolbox_module]=version(gr);
if nargin ~= 1
    error([toolbox_name ':' toolbox_module ':' 'wrongArgCount'],...
        'One input expected.');
end
if ~gr.directed
    error([toolbox_name ':' toolbox_module ':' 'graphNotDirected'],...
        'STOP only supported for directed graphs.');
end

% Degree of nodes
[degree,han] = deg(gr);

% Apply start node conditions
idx = degree(:,1) == 0;
han = han(idx);

if isempty(han)
    han = [];
end