%SELFLOOP  Edges the go from and to the same node.
%  [EDGE_HAN,NODE_HAN]=SELFLOOP(GRAPH) returns the list of edge handles
%  EDGE_HAN which go from and to the same node.  NODE_HAN is the list of
%  nodes which have these edges.
%
%  See also: graph.

% <USER_ID>
% </USER_ID>

function [edge_han,node_han] = selfloop(gr)

%<TOOLBOX_INFO>
TOOLBOX_NAME    = 'GraphToolbox';
TOOLBOX_MODULE  = 'Graph';
%</TOOLBOX_INFO>

if nargin ~= 1
    error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'wrongArgCount'],...
        'One input expected.'); 
end

% Find edges with from node == to node
idx = gr.edgelist(:,1) == gr.edgelist(:,2);
edge_han = gr.edgehandles(idx);
node_han = gr.nodehandles(gr.edgelist(idx,1));
