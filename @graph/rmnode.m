%RMNODE  Removes a node from the specified graph.
%  [GRAPH]=RMNODE(GRAPH,NODE_HANDLE)
%  [GRAPH]=RMNODE(GRAPH,NODE_NAME)
%
%  RMNODE deletes an node from a graph.  All other data assoicated with the
%  node is also deleted.
%
%  See also: graph/addnode, graph/addedge, graph/rmedge.
%

% <USER_ID>
% </USER_ID>

function [gr] = rmnode(gr,han)

%<TOOLBOX_INFO>
TOOLBOX_NAME    = 'GraphToolbox';
TOOLBOX_MODULE  = 'Graph';
%</TOOLBOX_INFO>

% Error check inputs
if nargin < 2,
    error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':'  'wrongArgCount'],...
        'Two inputs required.');
end

% Convert node name to handle if needed.
if ischar(han)
    han = name2handle(gr,'Node',han);
end

% Get index info
idx = handle2index(gr,'node',han);

% Execute callback.
gr = calleventfcn(gr,'pre-rmnode',han);

% Remove the edges which start and end at this node
[from,to] = edges(gr,han);
edge_han = [from to];
gr = rmedge(gr,edge_han);

% Eliminate the handles & nodes
% delete(gr.nodes(idx));
gr.nodehandles(idx) = [];
gr.nodes(idx)    = [];

% Execute callback.
gr = calleventfcn(gr,'post-rmnode',han);

% Update the value in the caller workspace
if length(inputname(1)) > 0,
    assignin('caller',inputname(1),gr);
end

