%PLUS  Graph addition.
%   G3=G1+G2 generates graph G3 with two components containing all of the
%   nodes and edges of G1 and G2 respectively.
%   
%   See also: graph, graph/comp.

% <AUTHOR>
% </AUTHOR>

function [g_new,nh,eh] = plus(g1,g2)

% Error check inputs
[toolbox_name,toolbox_module] = version(g1);
if nargin ~= 2
    error([toolbox_name ':' toolbox_module ':' 'wrongArgCount'],...
        'Two inputs expected.');
end
if ~isa(g2,'graph')
    error([toolbox_name ':' toolbox_module ':' 'wrongArgType'],...
        'G2 must be of type GRAPH.');
end    

% List an matrix appending.
g_new             = g1;
handle_offset     = g1.handlecount;
g_new.handlecount = g_new.handlecount + g2.handlecount;

% Add in node & edge objects.
g_new.nodes = [g_new.nodes; g2.nodes];
g_new.edges = [g_new.edges; g2.edges];

% Node & edge handles
nh = g2.nodehandles+handle_offset;
g_new.nodehandles = [g_new.nodehandles nh];
eh = g2.edgehandles+handle_offset;
g_new.edgehandles = [g_new.edgehandles eh];

% Edge list
g_new.edgelist = [g_new.edgelist; g2.edgelist+handle_offset];

g_new.name = [g1.name ' + ' g2.name];
