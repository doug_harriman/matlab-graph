%PLOT  Plot a graph.
%

% <AUTHOR>
% </AUTHOR>

function [obj2] = plot(obj,varargin)

% Promote to a graphui object and plot that.
obj2 = graphui(obj);
obj2 = plot(obj2,varargin{:});