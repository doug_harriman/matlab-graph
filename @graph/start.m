%START  List of nodes which initiate a directed graph.
%   [NODE_HAN]=START(GRAPH) returns a NODE_HAN which have an enter degree of
%   zero and a non-zero exit degree.  These are the nodes which "start" the
%   directed graph.
%
%   See also: graph/deg, graph/stop.

% <AUTHOR>
% </AUTHOR>

function [han] = start(gr)

% Error check inputs
[toolbox_name,toolbox_module]=version(gr);
if nargin ~= 1
    error([toolbox_name ':' toolbox_module ':' 'wrongArgCount'],...
        'One input expected.');
end
if ~gr.directed
    error([toolbox_name ':' toolbox_module ':' 'graphNotDirected'],...
        'START only supported for directed graphs.');
end

% Degree of nodes
[degree,han] = deg(gr);

% Apply start node conditions
idx = degree(:,2) == 0;
han = han(idx);

if isempty(han)
    han = [];
end