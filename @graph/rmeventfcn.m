%RMEVENTFCN  Removes an event handler function for a graph.
%   [GRAPH]=RMEVENTFCN(GRAPH,FCN)  Removes the function FCN as an event handler 
%   for GRAPH.  FCN may be either a function name string or a function
%   handle.
%
% See also: graph, addeventfcn, iseventfcn.
%

% <USER_ID>
% </USER_ID>

function [gr] = rmeventfcn(gr,fcn)

%<TOOLBOX_INFO>
TOOLBOX_NAME    = 'GraphToolbox';
TOOLBOX_MODULE  = 'Graph';
TOOLBOX_VERSION = 1;
%</TOOLBOX_INFO>

% Error check inputs
if nargin ~= 2
    error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'wrongArgCount'],...
        ['Two inputs required.']); 
end

% Convert function names to handles
if ischar(fcn)
    existval = exist(fcn);
    if ~any(existval == [2 3 5 6])
        error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'wrongArgType'],...
            ['String value is not function: ' fcn]);
    end
    
    fcn = str2func(fcn);
end

% Check function handle
if ~isa(fcn,'function_handle')
    error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'wrongArgType'],...
        ['Input is not a valid function handle.']); 
end
    
% Get full path to function & check against that for each.
test_fcn = functions(fcn);
test_fcn = test_fcn.file;

% Search for event function & remove if found.
n_fcn = length(gr.eventfcn);
for i = 1:n_fcn
    fcn = functions(gr.eventfcn{i});
    fcn = fcn.file;
    
    if strmatch(fcn,test_fcn,'exact')
        gr.eventfcn(i) = [];
        break;
    end
end

% Execute callback.
gr = calleventfcn(gr,'rmeventfcn',fcn);

% Update the value in the caller workspace
if length(inputname(1)) > 0,
    assignin('caller',inputname(1),gr);
end
