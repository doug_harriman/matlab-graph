%SETNODE  Set node data.
%   GRAPH=SETNODE(GRAPH,HANDLE,NODE)
%   GRAPH=SETNODE(GRAPH,HANDLE,PROPERTY,VALUE)
%

% TODO - Update help text.
% TODO - Support vectorized set?
% TODO - Support multiple property set.
% TODO - Support direct set of node to update full node object in graph.
% TODO - Update to class version function call.

% <AUTHOR>
% </AUTHOR>

function [gr] = setnode(gr,han,prop,val)

%<TOOLBOX_INFO>
TOOLBOX_NAME    = 'GraphToolbox';
TOOLBOX_MODULE  = 'Graph';
%</TOOLBOX_INFO>

% Error check inputs
if nargin < 3
    error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'wrongArgCount'],...
        'Three inputs required.'); 
end
if nargin > 4
    error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'wrongArgCount'],...
        'Four inputs required.'); 
end
if (nargin > 3) && ~ischar(prop)
    error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'wrongArgType'],...
        'Property must be specified with a string.'); 
end
if (nargin < 4) && ~isa(prop,'node')
    error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'wrongArgType'],...
        'Node input must be a node object.'); 
end

if numel(han) > 1
    han = unique(han);
    han(han==0) = [];   % Drop any zero handles
    for i = 1:length(han)
        setnode(gr,han(i),prop,val);
    end
else
    % Single node set.
    % Convert handles to indecies
    idx = handle2index(gr,'node',han);

    % Easy case: node input
    if isa(prop,'node')
        gr.nodes{idx} = prop;
    else
        % Set individual properties
        prop = lower(prop);
        gr.nodes{idx}.(prop) = val;
    end
end

if nargout < 1
    % Assign the object.
    assignin('caller',inputname(1),gr);
end