%GETEDGE  Get edge data.
%   EDGE=GETEDGE(GRAPH,HANDLE)
%   VALUE=GETEDGE(GRAPH,HANDLE,PROPERTY)
%
%  HANDLE may be an array of edge handles for the graph object.
%  This requires VALUE either be scalar or have N elements.
%

% <USER_ID>
% </USER_ID>

% TODO - Need to vectorize proprety extraction.
% TODO - Update to class version call.

function [val] = getedge(gr,han,prop)

%<TOOLBOX_INFO>
TOOLBOX_NAME    = 'GraphToolbox';
TOOLBOX_MODULE  = 'Graph';
%</TOOLBOX_INFO>

% Error check inputs
if nargin < 2
    error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'wrongArgCount'],...
        'Two inputs required.'); 
end
if nargin > 3,
    error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'wrongArgCount'],...
        'Maximim of three inputs.'); 
end
if (nargin > 2) && (~ischar(prop))
    error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'wrongArgType'],...
        'Property must be specified with a string.'); 
end

% Convert handles to indecies
idx = handle2index(gr,'edge',han);

% Return just edge that's all that was requested.
if nargin == 2
    if idx > 0
        val = gr.edges{idx};
    else
        val = [];
    end
    return;
end

% Pass through to the edge objects.
% REVISIT - Convert to use ARRAYFUN w/Matlab 2006a to vectorize.
n_han = numel(han);
val = cell(size(han));
prop = lower(prop);
for i = 1:n_han
    if idx(i)
        e = gr.edges{idx(i)};
        val{i} = e.(prop);
    end
end

% Force to non-cell if reasonable.
if n_han == 1
    val = val{1};
elseif ~iscellstr(val)
    % If non-str, fill any empty cells with zeros.
    idx = cellfun('isempty',val);
    val(idx) = {0};
    val = cell2mat(val);
end