%GETNODE  Direct access to node properties.
%  VALUE=GETNODE(GRAPH,HANDLE,PROPERTY)
%  NODE=GETNODE(GRAPH,HANDLE)
%  NODE=GETNODE(GRAPH,NAME)
%  HANDLE may be an array of edge handles for the graph object.
%  This requires VALUE either be scalar or have N elements.
%

% TODO - Need to vectorize proprety extraction.
% TODO - Update to class version call.

% <USER_ID>
% </USER_ID>

function [val] = getnode(gr,han,prop)

%<TOOLBOX_INFO>
TOOLBOX_NAME    = 'GraphToolbox';
TOOLBOX_MODULE  = 'Graph';
%</TOOLBOX_INFO>

% Error check inputs
if nargin < 2,
    error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'wrongArgCount'],...
        'Two inputs required.'); 
end
if nargin > 3,
    error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'wrongArgCount'],...
        'Maximim of three inputs.'); 
end
if (nargin > 2) && (~ischar(prop))
    error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'wrongArgType'],...
        'Property must be specified with a string.'); 
end

% Handle name and cell str inputs.
if isnumeric(han)
    han = double(han);
end
switch class(han)
    case 'double'
        % Do nothing
    case 'char'
        han = name2handle(gr,'node',han);
    case 'cell'
        han = name2handle(gr,'node',han);
    otherwise
        error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'wrongArgType'],...
            'Node must be specified by a node handle or name.');
end

% Convert handles to indecies
idx = handle2index(gr,'node',han);

if isempty(idx)
    error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'wrongArgValue'],...
        'Node specified does not exist in graph.');
end

% Return just node that's all that was requested.
if nargin == 2
    val = gr.nodes{idx};
    return;
end

% Get the property
n_node    = length(han);
node_list = gr.nodes(idx);
for i = 1:n_node
    n      = node_list{i};
    val{i} = n.(prop);
end

if n_node == 1
    val = val{1};
end

% Return row vector.
if ~ischar(val)
    val = val';
end