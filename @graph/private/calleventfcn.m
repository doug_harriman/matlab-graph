%CALLEVENTFCN  Calls event functions (graph private function).
%  

% <USER_ID>
% </USER_ID>

function [gr] = calleventfcn(gr,event,varargin)

%<TOOLBOX_INFO>
TOOLBOX_NAME    = 'GraphToolbox';
TOOLBOX_MODULE  = 'Graph';
TOOLBOX_VERSION = 1;
%</TOOLBOX_INFO>


if nargin ~= 3
    error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'wrongArgCount'],...
        ['Three inputs required.']); 
end

% Call all event functions in the list.
n_fcn = length(gr.eventfcn);
for i = 1:n_fcn
    gout = gr.eventfcn{i}(gr,event,varargin{:});
    
    if ~isa(gout,'graph')
        f = functions(gr.eventfcn{i});
        error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'eventError'],...
            ['Event handler did not return graph object:' f.function]);
    end

    % Apply event handler effects
    gr = gout;
end
