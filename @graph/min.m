%MIN  Minimum length path through graph.
%   [LENGTH,PATH]=MIN(GRAPH) calculates the minimum length path through
%   the GRAPH returning the path LENGTH and the actual PATH(s) which 
%   have that length.
%
%   [LENGTH,PATH]=MIN(GRAPH,DIM) calculates the minimum length path for
%   the edge weight DIM for graphs whose edges have multiple weights.  The
%   default value for DIM is 1.
%
%   See also: graph, graph/paths, graph/pathlen, graph/max.

% <USER_ID>
% </USER_ID>

function [len,node_path] = min(gr)

%<TOOLBOX_INFO>
TOOLBOX_NAME    = 'GraphToolbox';
TOOLBOX_MODULE  = 'Graph';
TOOLBOX_VERSION = 1;
%</TOOLBOX_INFO>

% Error check inputs
if nargin > 2
    error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'wrongArgCount'],...
        'Two inputs maximum.'); 
end
if nargin < 1
    error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'wrongArgCount'],...
        'One input required.'); 
end

% Defaults
if nargin < 2 
   dim = 1;
end

% Calc the path lengths & extract the longest.
[all_len,node_path] = pathlen(gr);
all_len = all_len(dim,:);
len = min(all_len);
idx = len == all_len;
node_path = node_path(:,idx);

% Drop any all zero rows.
if size(node_path,2) > 1
    idx = all(node_path')';
    node_path = node_path(idx,:);
else
    node_path(node_path==0) = [];
end
