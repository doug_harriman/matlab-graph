%COMP  Determine the disconnected components of a graph.
%   NM=COMP(GRAPH) generates a node matrix NM with rows containing the
%   nodes of each component in GRAPH.
%
%   See also: graph.

% <AUTHOR>
% </AUTHOR>

function [comp_mat] = comp(g)

%<TOOLBOX_INFO>
TOOLBOX_NAME    = 'GraphToolbox';
TOOLBOX_MODULE  = 'Graph';
%</TOOLBOX_INFO>

% Error check inputs
if nargin ~= 1
    error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'wrongArgCount'],...
        'One input expected.');
end

% Process the edge list, adding nodes to components as needed.
el = g.edgelist;
comp_mat     = [];
cur_comp_idx = 0;
cur_node_idx = 1;
cur_comp     = [];
while ~isempty(el)
    % Next node to process
    if cur_node_idx <= length(cur_comp)
        % Continue in this component
        cur_node = cur_comp(cur_node_idx);
    else
        % New component.
        cur_comp_idx = cur_comp_idx + 1;
        cur_node_idx = 1;
        cur_node = el(1,1);
        cur_comp = cur_node;
    end
        
    % Find the nodes adjacent to the current node.
    tf    = ismember(el,cur_node);
    [i,j] = find(tf);
    
    if ~isempty(i)
        % Add the adjacent nodes to the current component.
        j         = 1+~(j-1); % adjacent nodes
        new_nodes = unique(el(i,j));
        cur_comp  = [cur_comp; setdiff(new_nodes,cur_comp)];

        % Update component matrix
        comp_mat(1:length(cur_comp),cur_comp_idx) = cur_comp;
        
        % Remove those edges from the list.
        el = el(~any(tf,2),:);
    end
    
    % Process the next node.
    cur_node_idx = cur_node_idx + 1;
end

% Process any remaining, unconnected nodes.
con_nodes   = unique(comp_mat);
uncon_nodes = setdiff(g.nodehandles,con_nodes);
comp_mat(1,cur_comp_idx+[1:length(uncon_nodes)]) = uncon_nodes';
