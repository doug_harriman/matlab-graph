%EDGENAMES  Return a cell string array of all edges names in graph.
%   NAMELIST=EDGENAMES(GRAPHOBJ) returns a cell string array EDGELIST of
%   all of the names of the edges that exist in GRAPHOBJ.
%
%   See also: graph/getedge, edge/get.
%

% TODO - convert to using CELLFUN for Matlab 2006a and beyond.

% <USER_ID>
% </USER_ID>

function [namelist] = edgenames(gr)

%<TOOLBOX_INFO>
TOOLBOX_NAME    = 'GraphToolbox';
TOOLBOX_MODULE  = 'Graph';
%</TOOLBOX_INFO>

% Error checks
if nargin > 1
    error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'wrongArgCount'],...
        'One input expected.');
end

% Walk the nodelist, extracting the node names.
namelist = cellfun(@(x) x.name,gr.edges,'UniformOutput',false);
