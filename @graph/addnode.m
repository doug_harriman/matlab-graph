%ADDNODE  Adds a node to the specified graph.
%   [HANDLE,GRAPH]=ADDNODE(GRAPH) adds a single default node to GRAPH and
%   returns the HANDLE of the added node along with the updated GRAPH 
%   object.
%
%   [HANDLE,GRAPH]=ADDNODE(GRAPH,NUM_NODES) adds multiple default nodes to
%   GRAPH.
%
%   [HANDLE,GRAPH]=ADDNODE(GRAPH,NODE) adds the NODE object to the graph.
%   NODE may be a node array.
%
%   [HANDLE,GRAPH]=ADDNODE(GRAPH,NODE_PROP_1,NODE_VAL_1,...) adds a node to
%   GRAPH with the given node properties and property values.
%
%   See also: graph, node, graph/rmnode, graph/addedge.

% <AUTHOR>
% </AUTHOR>

% TODO - Support vectorized node via lists coming in.

function [new_handle,gr] = addnode(gr,varargin)

% Error check inputs
[toolbox_name,toolbox_module]=version(gr);
if nargin < 1
    error([toolbox_name ':' toolbox_module ':' 'wrongArgCount'],...
        'One input required.'); 
end

if nargin < 2
    % Add single node.
    varargin{1} = node;
end

% If 2 inputs, then either have:
% - multiple default nodes added
% - nodes added by passing in nodes objects or handles to node objects.
node_spec = varargin{1};
if length(varargin) == 1
    % Handle multiple default nodes 
    if isnumeric(node_spec)
        if (node_spec < 1) || isnan(node_spec) || isinf(node_spec) || ~isreal(node_spec)
            error([toolbox_name ':' toolbox_module ':' 'outOfRangeValue'],...
                'Number of new nodes must be a positive integer value.');
        end
        node_spec = uint32(node_spec);
    end
        
    % Add node by input class
    % Use the "if isa" construct instead of switch(class()) to allow
    % classes derived from node to be added too.
    if isa(node_spec,'node')
        % Don't need to do anything, but have case statement so can
        % catch and handle inputs of the wrong class.
    elseif isa(node_spec,'uint32')
        % Must be scalar
        if ~isscalar(node_spec),
            error([toolbox_name ':' toolbox_module ':' 'wrongArgSize'],...
                'Number of new nodes must be scalar.');
        end

        % Create an array of nodes.
        n_node    = node_spec;
        node_spec = node;
        for i = 1:n_node
            node_spec(i) = node;
        end

    else
        error([toolbox_name ':' toolbox_module ':' 'wrongArgType'],...
            'Unhandled input argument type.');
    end % switch class type
    
else
    % Nodes specified by by param/values
    node_spec = node(varargin{:});
end

% Add in new nodes
n_node = length(node_spec);

% Index of this node in the graph storage
idx = length(gr.nodehandles) + (1:n_node);

% Update the handle info.
new_handle          = gr.handlecount + (1:n_node);
gr.handlecount      = max(new_handle);
gr.nodehandles(idx) = new_handle;

% REVISIT - This should be vectorized.
for i = 1:length(new_handle)
    % Set handle in node.
    node_spec(i).handle = new_handle(i);
    
    % Set default name to handle if no name exists.
    if isempty(node_spec(i).name)
        node_spec(i).name = num2str(node_spec(i).handle);
    end
    
    % Add the nodes
    gr.nodes{idx(i),1} = node_spec(i);
end

% Execute callback.
gr = calleventfcn(gr,'addnode',new_handle);

% Update the value in the caller workspace
if nargout < 2
    assignin('caller',inputname(1),gr);
end
