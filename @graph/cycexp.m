%CYCEXP  Cyclic expansion of a graph.
%   GRAPH2=CYCEXP(GRAPH) removes the cycles of GRAPH by cyclic expansion.
%   Cyclic expansion adds additional nodes and edges to a graph so that 
%   cycles are removed.  It is assumed that each cycle would only be
%   traversed once, so a single copy of all edges and nodes in a cycle is
%   copied exactly once.
%
%   Example:
%
%   N0->N1->N2->N3 cycexp==>  N0->N1->N2->N1_copy->N2_copy->N3
%        ^  |                         |->N3
%        |_ |
%
%   See also: graph, dfs.

% <AUTHOR>
% </AUTHOR>

function g2 = cycexp(g)

% Error check inputs
[toolbox_name,toolbox_module] = version(g);
if nargin ~= 1
    error([toolbox_name ':' toolbox_module ':' 'wrongArgCount'],...
        'One input expected.');
end

% Calc cycles.
g_orig = g;
g2 = g;
[p,cyc] = dfs(g2);
while ~isempty(cyc)
    % Expand the first cycle
    cyc         = cyc(:,1);
    cyc(cyc==0) = [];
    [temp,i] = intersect(cyc,g.edgehandles);
    cyc_n = cyc;
    cyc_n(i) = [];
    cyc_e = edgepath(g,cyc_n);

    % Delete the last edge in the cycle, it will be replaced.
    cyc_e(cyc_e==0) = [];
    e = getedge(g2,cyc_e(end));
    rmedge(g2,cyc_e(end));

    % Extract the subgraph of all nodes & edges in the first cycle.
    orig_nodes = cyc_n(1:end-1);
    S.type = '()';
    S.subs{1} = orig_nodes;
    sub_g = g2.subsref(S);
   
    % Edges that go from the sub graph nodes to external to subgraph.
    % Book keep before we add subgraph back in.
    all_edges = edges(g2,orig_nodes);
    int_edges = edges(sub_g);
    ext_edges = setdiff(all_edges,int_edges);
    
    % Add that subgraph back in.
    [g2,new_nodes] = g2+sub_g;
    
    % Create a new edge from the last node in the cycle to the new subgraph.
    new_start = start(g2);
    new_start = new_start(end);
    addedge(g2,cyc_n(end-1),new_start,e);

    
    % Re-create the edges to connect the last node in the subgraph as it was
    %    connected in the original graph except the cycle edge.
    % New node that needs to be connected back in
    new_stop = stop(g2);
    new_stop = new_stop(end);

    % Now need to add the edges leaving the subgraph back in.
    % This means copying the original edge to a new place.
    for i = 1:length(ext_edges)
        % Get the edge to copy.
        e     = getedge(g2,ext_edges(i));
        
        % Map the from node to the new from node.
        [f,t] = nodes(g2,ext_edges(i));
        f = new_nodes(f==orig_nodes);
        
        % Add the node back in.
        addedge(g2,f,t,e);
    end
    
    % Get the cycles for the updated graph
    g = g2;
    [p,cyc] = dfs(g2);
    
end

% Update the name of the graph.
g2.name = ['Cyclic Expansion of ' g_orig.name];