%NODENAMES  Return a cell string array of all node names in graph.
%   NAMELIST=NODENAMES(GRAPH) returns a cell string array NODELIST of
%   all of the names of the nodes that exist in GRAPH.
%
%   See also: graph/getnode, node/get.
%

% <USER_ID>
% </USER_ID>

function [namelist] = nodenames(gr)

%<TOOLBOX_INFO>
TOOLBOX_NAME    = 'GraphToolbox';
TOOLBOX_MODULE  = 'Graph';
%</TOOLBOX_INFO>

% Error checks
if nargin > 1
    error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'wrongArgCount'],...
        'One input expected.');
end

% Walk the nodelist, extracting the node names.
namelist = cellfun(@(x) x.name,gr.nodes,'UniformOutput',false);
