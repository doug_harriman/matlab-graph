%ADJMAT  Calculate the adjacency matrix for a graph.
%   A=ADJMAT(GRAPH) calculats the adjacency matrix A for the graph G.  If G
%   is not directed, A will be upper triangular.
%
%   See also: graph.

% <AUTHOR>
% </AUTHOR>

% TODO - Return adjmat with handles.
% TODO - Return adjmat with edge weights.
% TODO - No caching is being done.  Do we really want/need it?

function [M,g] = adjmat(g)

% Error check inputs
[toolbox_name,toolbox_module]=g.version;
if nargin ~= 1
    error([toolbox_name ':' toolbox_module ':' 'wrongArgCount'],...
        'One input expected.');
end

% If we have an undirected graph, make upper triangular.
if ~g.directed
    g.edgelist = sort(g.edgelist,2);
    g.edgelist = unique(g.edgelist,'rows');
end

% Adjacency matrix is created on demand.
n_node = length(g.nodes);
if isempty(g.edgelist)
    % No edges in graph.
    M = sparse([]);
    M(n_node,n_node) = 0;
    return;
else
    % Extract the from and to indecies
    from = handle2index(g,'node',g.edgelist(:,1));
    to   = handle2index(g,'node',g.edgelist(:,2));

    % If graph is not directed, want just upper triangle full.
    if ~g.directed
        idx = [from to];
        idx = sort(idx,2);
        idx = unique(idx,'rows');
        from = idx(:,1);
        to   = idx(:,2);
    end

    % Create the adjacency matrix.
    idx = sub2ind([n_node n_node],from,to);
    M = sparse(n_node,n_node);
    M(idx) = 1;
end

% Return full matrix for smaller graphs.
if (n_node <= 10)
    % Convert to full matrix
    M = full(M);
end
