%HANDLE2INDEX Returns node or edge index given handle.
%  [IDX]=HANDLE2INDEX(GRAPH,TYPE,HAN) Converts the node or edge handle to an 
%  index into the approrpiate array.
%
%  See also: name2handle

% <USER_ID>
% </USER_ID>

function [idx] = handle2index(gr,type,han)

%<TOOLBOX_INFO>
TOOLBOX_NAME    = 'GraphToolbox';
TOOLBOX_MODULE  = 'Graph';
%</TOOLBOX_INFO>

% Error check inputs
if nargin ~= 3,
    error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'wrongArgCount'],...
        'Three inputs required.');
end

% Force handle in to a row vector.
if size(han,1) == 1
    han = han';
end

idx = [];  % Default values
switch(lower(type)),
    case 'node',
        % Vectorized lookup of multiple handles
        [tf,idx] = ismember(han,gr.nodehandles);
        if any(tf)
            idx(~tf) = 0;
        else
            idx = [];
        end

    case 'edge'

        % Vectorized lookup of multiple handles
        [tf,idx]  = ismember(han,gr.edgehandles);
        if any(tf)
            idx(~tf) = 0;
        else
            % No matches. Exit early.
            idx = [];
        end

    otherwise
        error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'unknownGraphChildType'],...
            ['Unsupported graph child type: ' upper(type)]);
end