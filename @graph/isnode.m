%ISNODE  Test whether a node exists in a graph.
%   TF=ISNODE(GRAPH,HANDLE)
%   TF=ISNODE(GRAPH,NAME) both return true if the given node HANDLE or node
%   NAME exist as a node in the GRAPH object.  HANDLE may be a vector of
%   handles and NAME may be a cell string array of names.
%
%   See also: graph, graph/addnode, graph/handle2index.
%

% <USER_ID>
% </USER_ID>

function [tf] = isnode(gr,id)

%<TOOLBOX_INFO>
TOOLBOX_NAME    = 'GraphToolbox';
TOOLBOX_MODULE  = 'Graph';
%</TOOLBOX_INFO>

% Error check inputs
if nargin ~= 2,
    error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'wrongArgCount'],...
        'Two inputs required.'); 
end

% Handle any numeric type
if isnumeric(id)
    id = double(id);
end

% Switch on input type
switch(class(id))
    case {'char','cell'}
        % Single node name input.
        [tf] = ismember(id,nodenames(gr));

    case 'double'
        % Single or list of node handles.
        [tf] = ismember(id,gr.nodehandles);

        % Allow 0 as a passthrough.
        tfz = ismember(id,0);
        tf = tf | tfz;

    otherwise
        error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'wrongArgType'],...
            ['Unsupported node specification type: ' class(id)]);
end