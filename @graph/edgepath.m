%EDGEPATH  Converts node paths to edge paths.
%  EDGE_PATH=EDGEPATH(GRAPH,NODE_PATH) generates an EDGE_PATH matrix with 
%  column vectors of edge handles given a NODE_PATH matrix of node path
%  vectors.  NODE_PATH is typically generated with GRAPH/PATHS.
%
%  Vectors are zero padded as needed.  
%
%  See also: graph, graph/paths, graph/pathlen.

% TODO - Complete help text.
% TODO - Doesn't handle parallel edges.  Have a hack in below.

% <USER_ID>
% </USER_ID>

function [path_edge] = edgepath(gr,node_path)

%<TOOLBOX_INFO>
TOOLBOX_NAME    = 'GraphToolbox';
TOOLBOX_MODULE  = 'Graph';
%</TOOLBOX_INFO>

% Error check inputs
if (nargin ~= 2)
    error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'wrongArgCount'],...
        'Two inputs required.');
end
if  ~all(isnode(gr,unique(node_path)))
    error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'wrongArgType'],...
        'Node handle in node path input not in graph.');
end
if ~gr.directed
    error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'graphNotDirected'],...
        'EDGEPATH only supported for directed graphs.');
end

% Calculate the edge paths.
path_edge = zeros(size(node_path,1)-1,size(node_path,2));
for col = 1:size(path_edge,2)
    for row = 1:size(path_edge,1)
        node_from = node_path(row,col);
        node_to   = node_path(row+1,col);
        
        if all([node_from node_to])
            edge_han  = edges(gr,node_from,node_to);

            if (edge_han == 0)
                error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'nonContinuousNodePath'],...
                    'Node path is not continuous.');
            end

            % HACK.  If have parallel edges, length(edge_han) will be > 1
            path_edge(row,col) = edge_han(1);
        end
    end
end
