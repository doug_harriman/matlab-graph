%RMEDGE  Removes an edge from the specified graph.
%  [GRAPH]=RMEDGE(GRAPH,EDGE_HANDLE)
%  [GRAPH]=RMEDGE(GRAPH,EDGE_NAME)
%  [GRAPH]=RMEDGE(GRAPH,NODE_FROM,NODE_TO) where NODE_FROM and NODE_TO each
%  may be either node or handles.
%
%  RMEDGE deletes an edge from a graph.  All other data assoicated with the
%  edge is also deleted.  
%
%  See also: graph/addedge, graph/addnode, graph/rmnode.
%

% TODO - Vectorize.

% <USER_ID>
% </USER_ID>

function [gr] = rmedge(gr,han,node_to)

%<TOOLBOX_INFO>
TOOLBOX_NAME    = 'GraphToolbox';
TOOLBOX_MODULE  = 'Graph';
%</TOOLBOX_INFO>

% Error check inputs
if nargin < 2
    error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':'  'wrongArgCount'],...
        'Two inputs required.');
end
if nargin > 3
    error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':'  'wrongArgCount'],...
        'Maximum of four inputs allowed.');
end

% If passed 2 nodes, convert to an edge handle.
if nargin == 3
    han = edges(gr,han,node_to);
end

% Catch null handles
if isempty(han)
    return;
end

% Convert edge name to handle if needed.
if ischar(han)
    han = name2handle(gr,'Edge',han);
end

% Clean up an duplicates & remove any 0 handles.
han = unique(han);
han(han==0) = [];

% If have multiple handles, call recursively.
n_han = length(han);
if length(han) > 1
    for i = 1:n_han-1
        gr = rmedge(gr,han(i));
    end
    han = han(end);
end
    
% Get index info
idx = handle2index(gr,'edge',han);

% Execute callback.
gr = calleventfcn(gr,'pre-rmedge',han);

% Delete the edge
% delete(gr.edges(idx));
gr.edges(idx)       = [];
gr.edgehandles(idx) = [];
gr.edgelist(idx,:)  = [];

% Execute callback.
gr = calleventfcn(gr,'post-rmedge',han);

% Update the value in the caller workspace
if length(inputname(1)) > 0,
    assignin('caller',inputname(1),gr);
end

