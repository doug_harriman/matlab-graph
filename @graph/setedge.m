%SETEDGE  Set edge data.
%  GRAPH=SETEDGE(GRAPH,HANDLE,PROPERTY,VALUE)
%  HANDLE may be an array of edge handles for the graph object.
%  This requires VALUE either be scalar or have N elements.
%

% TODO - Rework help text.  This is a passthrought fcn to edge/set
% TODO - Need to handle vectorized set without a Matlab loop.

% <USER_ID>
% </USER_ID>

function [gr] = setedge(gr,han,prop,val)

%<TOOLBOX_INFO>
TOOLBOX_NAME    = 'GraphToolbox';
TOOLBOX_MODULE  = 'Graph';
%</TOOLBOX_INFO>

% Error check inputs
if nargin < 3
    error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'wrongArgCount'],...
        'Three inputs required.'); 
end
if nargin > 4
    error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'wrongArgCount'],...
        'Four inputs required.'); 
end
if (nargin > 3) && ~ischar(prop)
    error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'wrongArgType'],...
        'Property must be specified with a string.'); 
end
if (nargin < 4) && ~isa(prop,'edge')
    error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'wrongArgType'],...
        'Edge input must be a edge object.'); 
end

if numel(han) > 1
    han = unique(han);
    han(han==0) = [];   % Drop any zero handles
    for i = 1:length(han)
        setedge(gr,han(i),prop,val);
    end
else
    % Single edge set.
    % Convert handles to indecies
    idx = handle2index(gr,'edge',han);

    % Easy case: edge input
    if idx > 0
        if isa(prop,'edge')
            gr.edges{idx} = prop;
        else
            % Set individual properties
            prop = lower(prop);
            gr.edges{idx}.(prop) = val;
        end
    end
end

if nargout < 1
    % Assign the object.
    assignin('caller',inputname(1),gr);
end
