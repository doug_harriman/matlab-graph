%ISBINTREE  Determine if a graph is a binary tree.
%  TF=ISBINTREE(GRAPH) Returns logical value TF depending on whether GRAPH
%  is a binary tree or not.  A graph is determined to be a binary tree if 
%  it is connected, directed, acyclic and all nodes have at most one edge 
%  entering and at most two edges exiting.
%
%  See also: graph/isconn, graph/iscyclic, graph/istree.
%

function tf = isbintree(gr)

%<TOOLBOX_INFO>
TOOLBOX_NAME    = 'GraphToolbox';
TOOLBOX_MODULE  = 'Graph';
%</TOOLBOX_INFO>

% Error check inputs
if nargin ~= 1
    error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'wrongArgCount'],...
        'One input expected.'); 
end

% Assume false
tf = false;

% Check connectivity
if ~isconn(gr)
    return;
end

% Check directed
if ~gr.directed
    return;
end

% Check acyclic
if iscyclic(gr)
    return;
end

% Check the edge incidence of each node.
inc = deg(gr);

% At most two outgoing edges.
if any(inc(:,1)>2)
    return;
end

% At most one incoming edge.
if any(inc(:,2)>1)
    return;
end

% Success, have a tree.
tf = true;
