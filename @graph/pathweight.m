%PATHWEIGHT  Caculates the edge weights along the specified paths in a graph.
%   [WEIGHTS,PATH]=PATHWEIGHT(GRAPH,PATH) calculate the WEIGHTS of the edges
%   along PATH in GRAPH.  If PATH is a path_length by num_paths sized two
%   dimentional matrix, and each edge contains N weights, then WEIGHTS is a
%   path_length by num_paths by N sized three dimensional matrix.
%
%   [WEIGHTS,PATH]=PATHWEIGHT(GRAPH) uses graph/dfs with default options to 
%   generate the paths. 
%
%   See also: graph, graph/dfs, graph/pathlen.

% <USER_ID>
% </USER_ID>

function [w,out_path] = pathweight(gr,path)

%<TOOLBOX_INFO>
TOOLBOX_NAME    = 'GraphToolbox';
TOOLBOX_MODULE  = 'Graph';
%</TOOLBOX_INFO>

% Error check inputs
if (nargin > 2)
    error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'wrongArgCount'],...
        'Two inputs maximum.');
end

% Defaults
if (nargin < 2) || isempty(path)
    path = dfs(gr);
    out_path = path;
end

% Remove nodes from paths & convert to edge indecies.
path(ismember(path,gr.nodehandles)) = 0;
path = path(any(path,2),:);
path = handle2index(gr,'edge',path);

% Extract the weights.
fcn = @(e) e.weight;
w   = mapedge(gr,fcn,'UniformOutput',false);
w   = cell2mat(w);

% Some paths are shorter than others, so are zero padded.
% Can't index into a matrix with a zero, so force a 1 in for now.
% We'll overwrite the weight values with zeros later.
idx_zero = path == 0;
path(idx_zero) = 1;
w   = w(path,:);
idx_zero = find(idx_zero);
w(idx_zero,:) = zeros(length(idx_zero),size(w,2));

% Now reshape the weights so that they're along the paths with each weight
% in it's own 2D matrix.
w   = reshape(w,size(path,1),size(path,2),[]);