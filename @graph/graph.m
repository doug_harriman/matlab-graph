%GRAPH  Graph class definition.
%

% <AUTHOR>
% </AUTHOR>

% TODO - Incident matrix access.
% TODO - Create graph from incidence & adjacency matrices.
% TODO - Caching scheme doesn't work.
% TODO - Move properties to appropriate levels of protection.

classdef graph
%% Public Properties
    properties
        % Main properties
        name     = 'Unnamed Graph';
        directed = true; % get public, set private (or just call method?)

        % Child objects
        handlecount = 0;  % Get/Set private
        nodehandles = []; % set private, get protected

        % Edgelist is main data structure.
        % Format: [edgehandle nodehandle1 nodehandle2] where edge goes from
        %         node 1 to node 2 in a digraph.
        edgelist    = []; % get public (through method), set private

        % Event handling
        % User event handler array.
        eventfcn      = {};  % set private, get public

        % Default edge weights so that can enforce weight size.
        defaultedgeweight = 1;
        
        % Other properties
        metadata      = struct(); % get/set public
        userdata      = []; % get/set public
    end

%% Public Properites - Protected Set
    properties (SetAccess=protected)
        % Child Objects
        % Have to start with 
        nodes = {};
        edges = {};

        edgehandles = []; % dynamically calculated from edgelist.
    end

%% Public Properites - Private Set & Get
    
    
%% Public Methods
    methods
        %------------------------------------------------------------------
        % Graph Constructor
        %------------------------------------------------------------------
        function obj = graph(varargin)
            % Default graph exists in obj at this point.
            if nargin > 0
                % Make sure we have even arg cnt
                if isodd(nargin)
                    error([toolbox_name ':' toolbox_module ':' 'badinput'],...
                        'Property/value pairs expected.');
                end
                
                % Set each property as requested
                props = properties(obj);
                for i = 1:2:nargin
                    % Input pair
                    prop = lower(varargin{i});
                    val  = varargin{i+1};
                    
                    % Check
                    if ~ismember(prop,props)
                        error([toolbox_name ':' toolbox_module ':' 'unknownproperty'],...
                            'Unknown property: %s',prop);
                    end
                    
                    % Set
                    obj.(prop) = val;
                end
            end
            
        end % graph.graph

        %------------------------------------------------------------------
        % Display
        %------------------------------------------------------------------
        function display(gr)
            disp(' ');
            disp(['Graph Name: ' gr.name]);

            if gr.directed,
                disp('Directed  : true');
            else
                disp('Directed  : false');
            end

            % Use the default size display.
            size(gr);
            disp(' ');
        end % graph.display

        %------------------------------------------------------------------
        % Display
        %------------------------------------------------------------------
        function obj = set.defaultedgeweight(obj,value)
            % Weights restricted to be 1xN vectors.
            [toolbox_name,toolbox_module]=version(obj);
            if min(size(value)) > 1
                error([toolbox_name ':' toolbox_module ':' 'wrongArgSize'],...
                    'Edge weights must be 1xN vectors.');
            end
            if size(value,1) > 1
                value = value';
                warning([toolbox_name ':' toolbox_module ':' 'wrongArgSize'],...
                    'Edge weights must be 1xN vectors.  Storing transpose.');
            end

            % Update default weight.
            obj.defaultedgeweight = value;
            
            % Now force weight vector size on any pre-existing edges.
            n_edge = length(obj.edges);
            nom_len = length(value);
            for i =1:n_edge
                e = obj.edges{i};
                
                % Fix short vectors
                if length(e.weight) < nom_len
                    e.weight(nom_len) = 0;
                end
                
                % Fix long vectors.
                if length(e.weight) > nom_len
                   e.weight = e.weight(1:nom_len); 
                end
                
                % Put the updated edge back.
                obj.edges{i} = e;
            end
        end
        
        %------------------------------------------------------------------
        % Version
        %------------------------------------------------------------------
        function [toolbox_name,toolbox_module,toolbox_version] = version(obj)
            %<TOOLBOX_INFO>
            TOOLBOX_NAME    = 'GraphToolbox';
            TOOLBOX_MODULE  = 'Graph';
            TOOLBOX_VERSION = 2;
            %</TOOLBOX_INFO>

            if nargout == 0
                disp(' ')
                disp(['Toolbox: ' TOOLBOX_NAME]);
                disp(['Version: ' num2str(TOOLBOX_VERSION)]);
                disp(['Module : ' TOOLBOX_MODULE]);
                disp(' ')
                return;
            end

            toolbox_name    = TOOLBOX_NAME;
            toolbox_module  = TOOLBOX_MODULE;
            toolbox_version = TOOLBOX_VERSION;
        end % graph.version

        %------------------------------------------------------------------
        % Subsref
        %------------------------------------------------------------------
        function [B] = subsref(A,S)
            
            % Catch user using dot notation to access methods.
            if length(S) > 1
                S = S(end);
            end
            
            switch S.type
                case '()'
                    % Get nodes to keep
                    han = S.subs{1};
                    node_han = intersect(han,A.nodehandles);

                    % Create the copy.
                    B = A;

                    % Remove all nodes that we don't need.
                    % This removes all of the unneeded edges too.
                    node_han = setdiff(A.nodehandles,node_han);
                    for i = 1:length(node_han)
                        % REVISIT - Vectorized rmnode didn't work. Eliminate loop when it does.
                        B = rmnode(B,node_han(i));
                    end

                    % If the indexes contained edges, keep just those
                    % edges.
                    edge_han = intersect(han,B.edgehandles);
                    if ~isempty(edge_han)
                        % Edges to remove
                        edge_han = setdiff(B.edgehandles,edge_han);
                        
                        % Remove those edges.
                        for i = 1:length(edge_han)
                           B = rmedge(B,edge_han(i)); 
                        end
                    end
                    
                    % Update the name
                    B.name = [B.name '(' num2str(B.nodehandles) ')'];

                case '.'
                    B = A.(S.subs);
                    
                otherwise
                    [toolbox_name,toolbox_module] = version(A);
                    error([toolbox_name ':' toolbox_module ':' 'illegalSyntax'],...
                        'Graph objects can not be referenced with this syntax: %s',S.type);
            end
        end % subsref
    end % methods
end % classdef