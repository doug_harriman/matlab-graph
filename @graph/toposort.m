%TOPOSORT  Topological sort of a graph.
%  [NODE_VEC]=TOPOSORT(GRAPH) topologically sorts GRAPH, generating an
%  ordered NODE_VEC.  NODE_VEC lists the nodes in the order in which they
%  should be visited so that all dependencies between nodes are satisfied.
%
%  Note that NODE_VEC is not a continuous path through the graph.  TOPOSORT
%  is typically used when the graph represents a dependency chart.
%
%  If GRAPH is unconnected, NODE_VEC will return one column per component
%  of GRAPH.
%
%  See also: graph, graph/comp.

% <USER_ID>
% </USER_ID>

function [path_vec] = toposort(g)

%<TOOLBOX_INFO>
TOOLBOX_NAME    = 'GraphToolbox';
TOOLBOX_MODULE  = 'Graph';
%</TOOLBOX_INFO>

% Error check inputs.
if nargin ~= 1
    error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'wrongArgCount'],...
        'One input expected.');
end

% Get the components of the graph.
comps  = comp(g);
n_comp = size(comps,2);

% Handle each component individually
path_vec = zeros(1,n_comp);
for i_comp = 1:n_comp
    % Extract the subgraph of interest.
    cur_comp = comps(:,i_comp);
    cur_comp(cur_comp==0) = [];
    S.type = '()';
    S.subs{1} = cur_comp;
    g2 = g.subsref(S);
    
    % Preallocate space for this component.
    path_vec(length(cur_comp),i_comp) = 0;
    
    % Keep removing first node with in degree of 0 until no nodes remain.
    % If nodes do remain, we have a cycle.
    sn  = start(g2);
    idx = 1;
    while ~isempty(sn)
        path_vec(idx,i_comp) = sn(1);
        g2  = rmnode(g2,sn(1));
        sn  = start(g2);
        idx = idx + 1;
    end
    
    % Catch cycles
    if length(g2.nodehandles) > 1
        error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'cyclicGraph'],...
            'Cycle detected in component %d of graph.',i_comp);
    end
    
    % Graph is now sorted correctly, however, graph start nodes (no
    % incoming edges) can be all bunched at the top of the sort order.
    % While this is techinically correct, it is non-optimal.  The next step
    % optimizes the location of the input degree 0 nodes such that they are
    % as low in the sort as possible.  For call order graphs, this
    % minimizes redundant calls.
    
    % Re-extract the subgraph of interest.
    cur_comp = comps(:,i_comp);
    cur_comp(cur_comp==0) = [];
    S.type = '()';
    S.subs{1} = cur_comp;
    g2 = g.subsref(S);
    
    % Find all start nodes.
    pv = path_vec(:,i_comp);
    deg0 = start(g2);
    list = g2.edgelist;
    if ~isempty(list) % Handle case where graph is fully disconnected.
        for i = 1:length(deg0)
            % Remove this start node from the sorted list.
            pv(pv == deg0(i)) = [];
            
            % Index of all nodes that are connected to this node.
            next_node = list(list(:,1) == deg0(i),2);
            next_node = unique(next_node);
            
            % Determine the highest point in the sort of any next_node.
            [tf,idx] = ismember(next_node,pv);
            idx = min(idx);  % New index for this deg0 node.
            
            % Re-insert this start node into the new position.
            pv = [pv(1:idx-1); deg0(i); pv(idx:end)];
        end
        path_vec(:,i_comp) = pv;
    end
end



