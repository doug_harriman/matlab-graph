%DEG  Degree of a node.
%   [DEGREE,NODE_HAN]=DEG(GRAPH) calculates a Nx1 DEGREE vector for each
%   node in the undirected GRAPH, where degree is the number of edges 
%   incident on the node.  NODE_HAN is the Nx1 vector of node handles for 
%   each node in the graph and has a one to one mapping to the DEGREE 
%   vector.
%
%   [DEGREE,NODE_HAN]=DEG(GRAPH,NODE_HAN) calculates the the DEGREE for
%   graph subset of nodes in NODE_HAN.
% 
%   [DEGREE,NODE_HAN]=DEGREE(GRAPH,NODE_HAN) where GRAPH is directed, 
%   returns the Nx2 vector of directed degree for graph.  DEGREE(n,1) is 
%   the number of edges exiting the node, while DEGREE(n,2) is the number 
%   of edges entering the node.
%
%   See also: graph, graph/adjmat, graph/incmat.

%<AUTHOR>
%</AUTHOR>

function [degree,node_han] = deg(gr,node_han)

% Error check inputs
[toolbox_name,toolbox_module]=version(gr);
if nargin > 1
    error([toolbox_name ':' toolbox_module ':' 'wrongArgCount'],...
        'Maximum of two inputs.');
end

% Default node handles if needed.
if nargin < 2
    node_han = gr.nodehandles;
else
    % Throw out any handles that aren't node handles.
    tf = ismember(node_han,gr.nodehan);
    node_han = node_han(tf);

    % Warn if some removed
    if ~all(tf)
        warning([toolbox_name ':' toolbox_module ':' 'wrongArgValue'],...
            'Invalid node handles removed.');
    end
    
    % Exit if no valid node handles.
    if ~any(tf)
        degree = [];
        node_han = [];
        return;
    end
end

% Assume directed graph and find in and out degrees.
node_han = unique(node_han);
n_node = length(node_han);
degree = zeros(n_node,2);
for i = 1:n_node
    if isempty(gr.edgelist)
        degree(i,:) = [0 0];
    else
        tf = ismember(gr.edgelist,node_han(i));
        degree(i,:) = sum(tf,1);
    end
end

% If non-directed, sum the two columns.
if ~gr.directed
    degree = sum(degree,2);
end