%NODES  Lists nodes in graph.
%   NODES(GRAPH) displays a list of the nodes in GRAPH.
%
%   [HANDLES,NAMES]=NODES(GRAPH) returns a list of the HANDLES and NAMES 
%   for each node in the graph.
%
%   [NODE_FROM,NODE_TO]=NODES(GRAPH,EDGE) returns the node handles for
%   the nodes which the given EDGE connects FROM and TO.  EDGE can be
%   specified as an edge name, as cell string array of names, a single edge
%   handle or a vector of edge handles.  If multiple edges are specified,
%   the returned nodes handles will be row vectors. If there are no edges, 
%   a zero will be returned.
%
%   See also: graph, edges, addnode, rmnode.
%

% <USER_ID>
% </USER_ID>

% TODO - Build up output by cols so everything will line up.
% TODO - Support third calling syntax which returns all nodes connected by the given edge.


function [varargout] = nodes(gr,edge)

%<TOOLBOX_INFO>
TOOLBOX_NAME    = 'GraphToolbox';
TOOLBOX_MODULE  = 'Graph';
%</TOOLBOX_INFO>

% Determine which version of the function to run based on the input/output
% signature.
if (nargin==1) && (nargout == 0)
    % Simple display of graph nodes.
    NodeDisplay(gr);
    
elseif (nargin==1) && (nargout > 0)
    % Data for all nodes in graph.
    [han,name]   = FullNodeList(gr);
    varargout{1} = han;
    varargout{2} = name;
    
elseif (nargin==2) 
    % Edges which go to/from requested nodes.
    [from,to]    = NodeConnections(gr,edge);
    varargout{1} = from;
    varargout{2} = to;
    
else
    % Error
    error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'wrongArgCount'],...
        'Wrong number of inputs/outputs.  See "help nodes" for more info.'); 
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Sub Functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%--------------------------------------------------------------------------
% NodeConnections
% Return list of node handles that the edge goes from and to.
%--------------------------------------------------------------------------
function [from,to] = NodeConnections(gr,edge)

%<TOOLBOX_INFO>
TOOLBOX_NAME    = 'GraphToolbox';
TOOLBOX_MODULE  = 'Graph';
%</TOOLBOX_INFO>

% Edge specification can be a single edge or multiple edges, and may be
% either names or handles.  Mixed names & handles not supported.
% Figure out what we have and convert to a vector of handles.
n_edge = numel(edge);
if isa(edge,'char')
    % Single node name specified.
    edge = name2handle(gr,'Edge',edge);
    
elseif isa(edge,'cell')
    % List of names, convert to handles.
    handles = zeros(n_edge,1);
    for i = 1:n_edge
        % TODO - Use vectorized call of NAME2HANDLE when implemented.
        han = name2handle(gr,'Edge',edge(i));
        if isempty(han)
            error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'unknownNode'],...
                ['Unknown edge name:' edge(i)]);
        end
        handles(i) = han;
    end
    edge = handles;
    
elseif isnumeric(edge)
    % List of edge handles, just validate.
    edge = reshape(edge,n_edge,1);  % Row vector.
    ind  = ismember(edge,gr.edgehandles);
    if ~all(ind)
        error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'unknownEdge'],...
            'Illegal edge handle specified.');
    end    
    
else
   % Unhandled data type 
    error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'wrongArgType'],...
        ['Unhandled data type for edge specification: ' class(edge)]); 
end

% Break down into list of edge handles into the nodes the edges go from and
% to.
tf   = ismember(gr.edgehandles,edge);
from = gr.edgelist(tf,1);
to   = gr.edgelist(tf,2);

%-------------------------------%--------------------------------------------------------------------------
% FullNodeList
% Return data for all nodes in graph.
%--------------------------------------------------------------------------
function [han,name] = FullNodeList(gr)

han    = gr.nodehandles;
name   = nodenames(gr)';

%--------------------------------------------------------------------------
% NodeDisplay
% Display list of nodes in graph.
%--------------------------------------------------------------------------
function [] = NodeDisplay(gr)

disp(' ');
[n_node,n_edge] = size(gr);
namelist = nodenames(gr);
for i = 1:n_node,
    disp(['  Node ' num2str(i) ': ' namelist{i}]);
end
disp(' ');