%ROOT  Returns root node handle for a tree graph.
%   [NODE_HAN]=ROOT(GRAPH) returns the NODE handle to the root node of a
%   tree graph.
%
%   See also: graph/istree, graph/getnode.
%

% <USER_ID>
% </USER_ID>

function [h] = root(gr)

%<TOOLBOX_INFO>
TOOLBOX_NAME    = 'GraphToolbox';
TOOLBOX_MODULE  = 'Graph';
%</TOOLBOX_INFO>

% Error check inputs
if nargin ~= 1
    error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'wrongArgCount'],...
        'One input expected.'); 
end

if ~istree(gr)
    error('Graph is not a tree');
end

% Get the edge incidence degrees
[d,h] = deg(gr);

% Find the node with no incident edges.
idx = find(d(:,2)==0,1);
h = h(idx);