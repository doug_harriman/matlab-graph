%Graph Toolbox
%
%Elementary graph manipulation.
%   graph        - Create a graph.
%   addnode      - Adds a node to the specified graph.
%   addedge      - Adds an edge to the specified graph.
%   rmnode       - NU. Removes a node from the specified graph.
%   rmedge       - NU. Removes an edge from the specified graph.
%
%Graph reduction.
%   fuse         - NI Fuse two nodes.
%   sred         - NI Series reduction of graph.
%   pred         - NI Paralled reduction of graph.
%   spred        - NI Repeated series and parallel reduction of a graph.
%
%Subgraph extraction.
%   circuits     - NI Closed paths of a graph. 
%   cycles       - NI Directed circuits of a graph.
%   comp         - NI Graph components for disconnected graphs.
%   spantree     - NI Spanning tree of a graph.
%
%Graph properties and property testing.
%   size         - Graph size.
%   max          - NU Maximum length path through graph.
%   min          - NU Minimum length path through graph.
%   isconnected  - NU Determines if a graph is connected.
%   iscyclic     - NU True for a graph which contains cycles.
%   isedge       - NU Test whether an edge exists in a graph.
%   isnode       - NU Test whether a node exists in a graph.
%
%Other graph operations.
%   dfs          - NU Depth first search of a graph.
%   toposort     - NU Topological sort of a graph.
%   pathlen      - NU Caculates the path length for a path through a graph.
%   selfloop     - NU Edges the go from and to the same node.
%   isolated     - NI  Return isolated nodes.
%   start        - List of nodes which initiate a directed graph.
%   stop         - List of nodes which terminate a directed graph.
%   deg          - Degree of a nodes.
%   edgepath     - NU Converts node paths to edge paths.
%   adjmat       - Calculate adjacency matrix for graph.
%   incmat       - NI Calculate incidence matrix for graph.     
%
%Special graphs.
%   kuratowski   - NI Kuratowski's non-planar graphs.
%   complete     - NI Complete graphs.
%   randgraph    - NI Random graphs.
%
%Visualization
%   plot         - NU Plot a graph.
%   layout       - NU Generate a specialized layout for a graph before plotting.
%   display      - NU Graph display to command window.
%
%Graphs as data structures.
%   map          - NU Maps a function over nodes or edges in a graph.
%   addeventfcn  - NU Adds an event handler function for a graph.
%   rmeventfcn   - NU Removes an event handler function for a graph.
%   iseventfcn   - NU Returns true if the given function is an event handler for graph.
% 
%Graph object properties.
%   get          - NI Get graph object properties.
%   set          - NI Set graph object properties.
%   setedge      - NU Set edge data.
%   getedge      - NU Get edge data.
%   setnode      - NU Set node data.
%   getnode      - NU Direct access to node properties.
%   nodes        - NU Lists nodes in graph.
%   edges        - NU List edges in graph.
%   nodenames    - NU Return a cell string array of all node names in graph.
%   edgenames    - NU Return a cell string array of all edges names in graph.
%   name2handle  - NU Returns node or edge handle given name.
%   handle2index - Returns node or edge index given handle.
%   char         - NU Graph to character conversion.
% 








