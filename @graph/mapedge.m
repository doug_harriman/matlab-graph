%MAPEDGE  Maps a function over all edges in a graph.
%  RESULT=MAPEDGE(GRAPH,FCN_HAN) maps the FCN_HAN over all of the
%  edges of the GRAPH returning result.
%
%  RESULT=MAPEDGE(GRAPH,FCN_HAN,'UniformOutput',false) removes the uniform
%  output restriction.  See CELLFUN.
%
% >> map(g,@fcn)
% fcn(graph,edge)
%
% See also: graph, mapnode, cellfun.

% <AUTHOR>
% </AUTHOR>

% TODO - Provide passthrough for UniformOutput.
% TODO - Provide method for allowing fcn with no output.
% TODO - Update help text.

function [res] = mapedge(obj,fcn,varargin)

% Error check inputs
[toolbox_name,toolbox_module] = version(obj);
if nargin < 2
    error([toolbox_name  ':' toolbox_module ':' 'wrongArgCount'],...
        'Two inputs required.'); 
end

% Default options.
opt.uniformoutput = true;
opt = options(opt,varargin{:});

% Apply function
res = cellfun(fcn,obj.edges,'UniformOutput',opt.uniformoutput);