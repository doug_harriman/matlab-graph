%ISCYCLIC  True for a graph which contains cycles.
%  [TF,CYCLE_EDGE,CYCLE_NODE]=ISCYCLIC(GRAPH) returns TF of true if GRAPH 
%  contains any cycles.  If cycles are detected, CYCLE_EDGE is a matrics 
%  where the column vectors contain lists of edges which form each cycle
%  while CYCLE_NODE lists the nodes which form each cycle. The number of 
%  columns in CYCLE_EDGE and CYCLEN_NODE is equal to the number of cycles
%  detected in the graph.  
%
%  Undirected graphs are always cyclic if they contain any edges.
%
%  See also: graph, graph/dfs, graph/edgepath.

% <USER_ID>
% </USER_ID>

function [tf,cycle] = iscyclic(gr)

%<TOOLBOX_INFO>
TOOLBOX_NAME    = 'GraphToolbox';
TOOLBOX_MODULE  = 'Graph';
%</TOOLBOX_INFO>

% Error check inputs.
if (nargin > 1)
    error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'wrongArgCount'],...
        'One input maximum.');
end

% Non-directed graphs are always cyclic if they have any edges.
[n_node,n_edge] = size(gr);
if ~gr.directed && (n_edge>0)
    tf = true;
end

% Get the cyclic paths.
[p,cycle] = dfs(gr);
if isempty(cycle)
    tf = false;
    return;
end

% If got here, have cycles.
tf = true;