%ADDEVENTFCN  Adds an event handler function for a graph.
%   [GRAPH]=ADDEVENTFCN(GRAPH,FCN)  Adds the function FCN as an event handler 
%   for GRAPH.  FCN may be either a function name string or a function
%   handle.
%
% See also: graph, rmeventfcn, iseventfcn.
%

% <USER_ID>
% </USER_ID>

function [gr] = addeventfcn(gr,fcn)

%<TOOLBOX_INFO>
TOOLBOX_NAME    = 'GraphToolbox';
TOOLBOX_MODULE  = 'Graph';
TOOLBOX_VERSION = 1;
%</TOOLBOX_INFO>

% Error check inputs
if nargin ~= 2
    error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'wrongArgCount'],...
        ['Two inputs required.']); 
end

% Convert function names to handles
if ischar(fcn)
    existval = exist(fcn);
    if ~any(existval == [2 3 5 6])
        error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'wrongArgType'],...
            ['String value is not function: ' fcn]);
    end
    
    fcn = str2func(fcn);
end

% Check function handle
if ~isa(fcn,'function_handle')
    error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'wrongArgType'],...
        ['Input is not a valid function handle.']); 
end
    
% Don't allow same function to be added multiple times.
if iseventfcn(gr,fcn)
    warning([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'illegalValue'],...
        ['Event handler function already in graph event handler list.']); 
    return;
end

% Add handler to graph
if isempty(gr.eventfcn)
    gr.eventfcn = {fcn};
else
    gr.eventfcn = {gr.eventfcn; fcn};
end

% Execute callback.
gr = calleventfcn(gr,'addeventfcn',fcn);

% Update the value in the caller workspace
if length(inputname(1)) > 0,
    assignin('caller',inputname(1),gr);
end
