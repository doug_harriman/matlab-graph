%NAME2HANDLE  Returns node or edge handle given name.
%   HAN=NAME2HANDLE(GRAPH,TYPE,NAME) returns the handle of the given node or edge
%   (specified in TYPE) with the given NAME.
%
% See also: handle2index.
%

% <USER_ID>
% </USER_ID>

% TODO - Support calling syntax where just pass in name.  
%        Would find node name first if there's a conflict.

function [han] = name2handle(gr,type,name)

%<TOOLBOX_INFO>
TOOLBOX_NAME    = 'GraphToolbox';
TOOLBOX_MODULE  = 'Graph';
TOOLBOX_VERSION = 1;
%</TOOLBOX_INFO>

% Error check inputs
if (nargin<3)
    error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'wrongArgCount'],...
        'Three inputs expected.');
end
if ~isa(type,'char')
    error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'wrongArgType'],...
        'Type specification must be a string of value either ''node'' or ''edge''.');
end
if size(type,1) > 1
    error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'wrongArgSize'],...
        'Type specification must be single line.');
end
if ~isa(name,'char') && ~isa(name,'cell')
    error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'wrongArgType'],...
        'Name specification must string or cell string array.');
end
if size(name,1) > 1
    error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'wrongArgSize'],...
        'Name specification must be single line.');
end

han = [];  % Default value
switch(lower(type))
    case 'node'
        % Find the matching names.
        [tf,idx] = ismember(name,nodenames(gr));

        % Convert the index to a handle.
        if ~isempty(idx) && all(idx)
            han = gr.nodehandles(idx);
        end
        
    case 'edge'
        % Find the matching names.
        [tf,idx] = ismember(name,edgenames(gr));
        
        % Convert the index to a handle.
        if ~isempty(idx) && all(idx)
            han = gr.edgehandles(idx);
        end
        
    otherwise
        error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'unknownGraphChildType'],...
            ['Unsupported graph child type: ' upper(type)]);
end