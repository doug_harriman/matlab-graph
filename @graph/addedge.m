%ADDEDGE  Adds an edge to the specified graph.
%   [HANDLE,GRAPH,INDEX]=ADDEDGE(GRAPH,NODE_FROM,NODE_TO) adds an edge to
%   GRAPH between NODE_FROM and NODE_TO.  NODE_FROM and NODE_TO may be
%   specified either by name or by handle.  The HANDLE of the inserted edge
%   is returned along with the updated graph object.
%
%   [HANDLE,GRAPH,INDEX]=ADDEDGE(GRAPH,NODE_FROM,NODE_TO,EDGE) inserts the
%   EDGE object as the edge in the graph.
%
%   [HANDLE,GRAPH,INDEX]=ADDEDGE(GRAPH,NODE_FROM,NODE_TO,EDGE_PROP,EDGE_VAL...)
%   creates a new edge object with the given propertie values.
%
%   If the graph is not directed (the DirectedEdges property is off), then
%   adding a node from 3->1 is equivalent to adding a node 1->3.
%
%   See also: graph, edge, graph/rmedge, graph/addnode.

% <USER_ID>
% </USER_ID>

% TODO - Do we need to return IDX still?
% TODO - Support syntax to allow adding vectorized edge insertion.
%        User to specifiy vectors of from and to nodes.
% TODO - How to deal with node handle being passed in as a string.
%        Issue: plot currently labels a node with its handle if the node
%        has not been named.  Thus to the user, it looks like this is the
%        node name.  So it seems reasonable that they would try to specify
%        this "name" as the node name.

function [new_handle,gr,idx] = addedge(gr,node_from,node_to,varargin)

% Error check inputs
[toolbox_name,toolbox_module]=version(gr);
if nargin < 3
    error([toolbox_name ':' toolbox_module ':' 'wrongArgCount'],...
        'Three inputs required.');
end

% Node validation.
if ischar(node_from),
    temp = name2handle(gr,'node',node_from);
    if isempty(temp),
        error([toolbox_name ':' toolbox_module ':' 'unknownNode'],...
            ['Node "' node_from '" not member of graph "' char(gr) '".']);
    end
    node_from = temp;
end
if ischar(node_to),
    temp = name2handle(gr,'node',node_to);
    if isempty(temp),
        error([toolbox_name ':' toolbox_module ':' 'unknownNode'],...
            ['Node "' node_to '" not member of graph "' char(gr) '".']);
    end
    node_to = temp;
end
if ~isnumeric(node_from) || ~isnumeric(node_to),
    error([toolbox_name ':' toolbox_module ':' 'wrongArgType'],...
        'Nodes must be specified by name or index.');
end

% Handle undirected graph case.
if ~gr.directed,
    temp = sort([node_from, node_to]);
    node_from = temp(1);
    node_to   = temp(2);
end

% Set default edge weight.
if length(varargin) ~= 1
    params = varargin(1:2:end);
    params = lower(params);
    tf     = ismember(params,'weight');
    if any(tf)
        % Weight was specified.  Make sure it conforms.
        idx = find(tf)*2;
        val = varargin{idx};
        if ~all(size(val) == size(gr.defaultedgeweight))
            [r,c] = size(gr.defaultedgeweight);
            error([toolbox_name ':' toolbox_module ':' 'wrongArgSize'],...
                'Edge weights must be vectors of size %dx%d for this graph.',r,c);
        end
    else
        % No weight specified.  Set to default.
        varargin{end+1} = 'weight';
        varargin{end+1} = gr.defaultedgeweight;
    end
end

% Create the new edge if needed.
if length(varargin) > 0
    % User specified some data about the edge
    if ~isa(varargin{1},'edge')
        new_edge = edge(varargin{:});
    else
        new_edge = varargin{1};

        % Currently don't handle input of multiple edges or an edge array.
        if length(varargin) > 1
            error([toolbox_name ':' toolbox_module ':' 'wrongArgSize'],...
                'Can only add one edge at a time.');
        end
    end

else
    % Just create a default edge.
    new_edge = edge;
end

% Create the new handle
new_handle      = gr.handlecount + 1;
new_edge.handle = new_handle;
gr.handlecount  = new_handle;

% Num edges is equal to the unique number of node handles that are
% non-zero.
n_edge = length(gr.edges);

% Add the edge to the list.
idx = n_edge + 1;
gr.edgehandles(idx) = new_handle;
gr.edgelist(idx,:)  = [node_from node_to];
gr.edges{idx,1}     = new_edge;

% Execute callback.
gr = calleventfcn(gr,'addedge',new_handle);

% Update the value in the caller workspace
if length(inputname(1)) > 0,
    assignin('caller',inputname(1),gr);
end

