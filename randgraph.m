%RANDGRAPH  Generates a random graph with the specified number of nodes.
%  GRAPH=RANDGRAPH(NUM_NODES,NUM_EDGES) generates a random graph with the
%  specified NUM_NODES and NUM_EDGES.  This is typically used for
%  validating graph algorithms against a variety of inputs.
%
%  GRAPH=RANDGRAPH(NUM_NODES) uses a default value of
%  NUM_EDGES=NUM_NODES^1.5.
%
%  GRAPH=RANDGRAPH(NUM_NODES,PARAM,VALUE,...) or
%  GRAPH=RANDGRAPH(NUM_NODES,NUM_EDGES,PARAM,VALUE,...) accepts optional
%  random graph generation options.  Supported options are:
%  AllowOrphans   - Boolean value.  Allow orphan nodes.  Default is true.
%  AllowSelfLoops - Boolean value.  Allow edges that connect from and to
%                    the same node.  Default is true.
%
%  See also: graph, graph/selfloop.

% TODO - Refactor orphans into own @graph function
% TODO - Add option on whether or not to support disconnected graphs.

% <USER_ID>
% </USER_ID>

function [g] = randgraph(n_nodes,varargin)

%<TOOLBOX_INFO>
TOOLBOX_NAME    = 'GraphToolbox';
TOOLBOX_MODULE  = 'Graph';
TOOLBOX_VERSION = 1;
%</TOOLBOX_INFO>

% Default inputs
if nargin > 1
    if isnumeric(varargin{1})
        n_edges = varargin{1};
        varargin = varargin(2:end);
    else
        n_edges = round(n_nodes^1.5);
    end
end
if (nargin < 2)
    n_edges = round(n_nodes^1.5);
end

% Default options
opt.alloworphans   = true;  % Nodes with no edges.  Will remove orphan nodes if false.
opt.allowselfloops = true;  % Edges to and from the same node.  Will remove self loops if false.

% Apply user options
[opt,cl,unhandled] = options(opt,varargin{:});
if ~isempty(unhandled)
    error([TOOLBOX_NAME ':' TOOLBOX_MODULE ':' 'wrongArgValue'],...
        ['Illegal option: ' unhandled{1}]); 
end    

% Generate the graph.
g = graph('Name','Random Graph');
addnode(g,n_nodes);
nh = nodes(g);

% Generate random edge indecies.
max_ind = n_nodes^2;
ind = randperm(max_ind);
ind = ind(1:n_edges);
[from,to] = ind2sub([1 1]*n_nodes,ind);

% Add the edges.
for i = 1:n_edges
    addedge(g,from(i),to(i));
end

% Apply options.
if ~opt.allowselfloops
    % Remove edges that go from and to the same node.
    eh = selfloop(g);
    rmedge(g,eh);
end

% Do orphan check second in case the only edge to/from a node was a self
% edge.
if ~opt.alloworphans
    % Search for orphan nodes and remove.
    [from,to] = edges(g,nh);
    orph_ind = ~(any(from')' | any(to')');
    orph_han = nh(orph_ind);
    
    rmnode(g,orph_han);
end
    
