%TOSTATE  Generate commands required to drive to a given state.
%  COMMANDS=TOSTATE(GRAPH,TARGET_STATE) generates a list of commands
%  required to drive a finite state machine represented by GRAPH to
%  TARGET_STATE.  It is assumed that the current state of the machine is
%  encoded in GRAPH.
%

% Doug Harriman

function [cmd_list] = tostate(g,s2)

% Figure out the current state for the component where the target state is
% specified.
s1 = CurState(g,s2);

% Initial command list
cmd_list = s2s(g,s1,s2)

% Convert external state transitions into input commands.
idx = strmatch('en(',cmd_list);
while ~isempty(idx)
    % Get the first command
    cmd_idx = idx(1);
    cmd     = cmd_list{cmd_idx};
    
    % Extract the target state
    s2 = strrep(cmd,'en(','');
    s2 = strrep(s2,')','');
    
    % Get the current state
    [s1,comp] = CurState(g,s2);
    
    % Get the commands
    cmd = s2s(g,s1,s2);

    % Replace the commands
    if cmd_idx == 1
        cmd_list = [cmd;cmd_list(2:end)];
    elseif cmd_idx == length(cmd_list)
        cmd_list = [cmd_list(1:end-1);cmd];
    else
        cmd_list = [cmd_list(1:cmd_idx-1);cmd;cmd_list(cmd_idx+1:end)];
    end
    
    % Update the current state
    han = name2handle(g,'Node',comp);
    setnode(g,han,'UserData',s2);
    
    % Next command to process
    idx = strmatch('en(',cmd_list);
end


% Returns current state of graph with a given state.
function [s1,comp_name] = CurState(g,s2)
idx = findstr(s2,'.');
comp_name = ['.' s2(1:idx-1)];
s1 = getnode(g,comp_name,'UserData');
