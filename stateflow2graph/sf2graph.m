%SF2GRAPH  Stateflow to Graph object converter.

function [g] = sf2graph

chart_name = 'Mech';

% Get base chart
r = sfroot;  % Stateflow root
c=r.find('-isa','Stateflow.Chart','name',chart_name);  

% Get all states.
states = c.find('-class','Stateflow.State');

% Add all states.
g = graph('Name',chart_name);
for i = 1:length(states)
    addnode(g,StateName(states(i)));
end

% Add the edges.
trans=c.find('-class','Stateflow.Transition');
for i = 1:length(trans)
    from_obj  = trans(i).Source;
    to_obj    = trans(i).Destination;
    
    if ~isempty(from_obj) && ~isempty(to_obj)
        from_name = StateName(trans(i).Source);
        to_name   = StateName(trans(i).Destination);
        addedge(g,from_name,to_name,'Userdata',trans(i).LabelString);
    end
end


function [full_name] = StateName(state)

comp_name = strrep(state.Path,state.Chart.Path,'');
comp_name = comp_name(2:end);
comp_name = strrep(comp_name,'\','.');

full_name = [comp_name '.' state.Name];