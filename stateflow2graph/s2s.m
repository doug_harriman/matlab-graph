%S2S  Commands required to drive FSM between given states.
%  COMMANDS=S2S(GRAPH,STATE1,STATE2) generates the list of commands
%  reqruired to drive the FSM represented by GRAPH from STATE1 to STATE2.
%

% Doug Harriman

function [cmd] = s2s(g,s1,s2)

% Generate the path between the given nodes.
path = dfs(g,s1,s2);

% Pick the shortest path
path = path(:,1);  % REVISIT - Just picking first path.

% Get the edges
path = edgepath(g,path);

% Get the first level commands:
cmd = getedge(g,path,'userdata');
