%LINEUI  LineUI class definition.
%

% <AUTHOR>
% </AUTHOR>

classdef (CaseInsensitiveProperties=true) lineui
    %% Properties
    properties % Get/set public
        % Handle graphics text object properties.
        color           = [0 0 0];
        linestyle       = '-';
        linewidth       = 0.5;
        marker          = 'none';
        markersize      = 6;
        markeredgecolor = 'auto';
        markerfacecolor = 'none';
        xdata           = [0 1];
        ydata           = [0 1];
        bez_calculated  = false;
        zdata           = [];
        visible         = 'on';
        userdata        = [];
        
        % UI Controls
        uicontextmenu = [];
        buttondownfcn = '';

        % Non handle graphics properties
        selected          = false;
        selectedcolor     = 'b';
        selectedlinesytle = '-';
        selectedlinewidth = 0.5;
    end

    properties (SetAccess='private')
        handle = [];
    end
    
    methods
        %------------------------------------------------------------------
        % Constructor
        %------------------------------------------------------------------
        function obj = lineui(varargin)
            if nargin > 0
                % Put the options into a struct.
                S = struct(obj);
                [S,changes,unhandled] = options(S,varargin{:});

                % Apply the changes.
                n_changes = length(changes);
                for i = 1:n_changes
                    change = changes{i};
                    obj.(change) = S.(change);
                end

                % Warn on the unhandled properties.
                n_unhandled = length(unhandled);
                [toolbox_name,toolbox_module] = version(obj);
                for i = 1:2:n_unhandled
                    prop = unhandled{i};
                    warning([toolbox_name ':' toolbox_module ':' 'unknownProperty'],...
                        'Unknown %s property (ignored): %s',class(obj),upper(prop));
                end
            end
        end

        %------------------------------------------------------------------
        % Plot
        %------------------------------------------------------------------
        function [handle,obj] = plot(obj,parent)
            % Make sure we plot in parent if provided.
            if nargin > 1
                axes(parent);
            end

            % Create the text object.
            obj.handle = line();
            handle = obj.handle;

            % Set it's fields
            obj = obj.update;

            if nargout < 2
                try
                    assignin('caller',inputname(1),obj);
                end
            end

        end


        %------------------------------------------------------------------
        % Bezier
        % Convert X,Y points to Bezier curve.
        %------------------------------------------------------------------
        function [obj] = bezier(obj)
            if ~obj.bez_calculated
                xy  = [obj.xdata' obj.ydata'];
                bez = bezier(xy')';
                yy = bez(:,2);
                xx = bez(:,1);

                obj.xdata = xx';
                obj.ydata = yy';
                
                obj.bez_calculated = true;
            end
        end


        %------------------------------------------------------------------
        % Version
        % TODO: make private.
        %------------------------------------------------------------------
        function [toolbox_name,toolbox_module,toolbox_version] = version(obj)
            %<TOOLBOX_INFO>
            TOOLBOX_NAME    = 'GraphToolbox';
            TOOLBOX_MODULE  = 'LineUI';
            TOOLBOX_VERSION = 1;
            %</TOOLBOX_INFO>

            if nargout == 0
                disp(' ')
                disp(['Toolbox: ' TOOLBOX_NAME]);
                disp(['Version: ' num2str(TOOLBOX_VERSION)]);
                disp(['Module : ' TOOLBOX_MODULE]);
                disp(' ')
                return;
            end

            toolbox_name    = TOOLBOX_NAME;
            toolbox_module  = TOOLBOX_MODULE;
            toolbox_version = TOOLBOX_VERSION;
        end % graph.version

        %------------------------------------------------------------------
        % Update
        % Updates GUI object if it is valid.
        %------------------------------------------------------------------
        function [obj] = update(obj)
            % Early exit if don't have GUI object.
            if ~ishandle(obj.handle)
                return;
            end
            if isempty(obj.handle)
                return;
            end
            
            % Set all properties that transfer
            props = properties(obj);
            for i = 1:length(props)
                if isprop(obj.handle,props{i})
                    try
                        set(obj.handle,props{i},obj.(props{i}));
                    end
                end
            end
        end

        %------------------------------------------------------------------
        % Delete
        % Deletes the GUI object
        %------------------------------------------------------------------
        function [obj] = delete(obj)
            if ishandle(obj.handle)
                delete(obj.handle);
                obj.handle = [];
            end
        end


        %% Property Set Functions
        %------------------------------------------------------------------
        % VersionWarning
        % Check Matlab version warn when function should be updated.
        %------------------------------------------------------------------
        function obj = version_warning(obj)
            return;
            v = ver('Matlab');
            d = datenum(v.Date);
            if d > 732668
                % Property set currently implemented with a double set &
                % update call.  Should use a PostSet event handler when
                % events are implemented in Matlab release 2006b.
                warning('Property set needs updating.');
            end
        end

        %------------------------------------------------------------------
        % Set
        % Set functions for each individual field.
        %------------------------------------------------------------------
        function obj = set.color(obj,value)
            obj.version_warning;
            obj.color = value;
            obj.update;
        end

        function obj = set.marker(obj,value)
            obj.version_warning;
            obj.marker = value;
            obj.update;
        end

        function obj = set.markeredgecolor(obj,value)
            obj.version_warning;
            obj.markeredgecolor = value;
            obj.update;
        end

        function obj = set.markerfacecolor(obj,value)
            obj.version_warning;
            obj.markerfacecolor = value;
            obj.update;
        end

        function obj = set.markersize(obj,value)
            obj.version_warning;
            obj.markersize = value;
            obj.update;
        end

        function obj = set.linestyle(obj,value)
            obj.version_warning;
            obj.linestyle = value;
            obj.update;
        end

        function obj = set.linewidth(obj,value)
            obj.version_warning;
            obj.linewidth = value;
            obj.update;
        end

        function obj = set.xdata(obj,value)
            obj.version_warning;
            obj.xdata = value;
            obj.update;
        end

        function obj = set.ydata(obj,value)
            obj.version_warning;
            obj.ydata = value;
            obj.update;
        end

        function obj = set.zdata(obj,value)
            obj.version_warning;
            obj.zdata = value;
            obj.update;
        end

        function obj = set.visible(obj,value)
            obj.version_warning;
            obj.visible = value;
            obj.update;
        end

        function obj = set.buttondownfcn(obj,value)
            obj.version_warning;
            obj.buttondownfcn = value;
            obj.update;
        end

        function obj = set.uicontextmenu(obj,value)
            obj.version_warning;
            obj.uicontextmenu = value;
            obj.update;
        end
        
        function obj = set.selected(obj,value)
            switch value
                case true
                    set(obj.handle,'Color',obj.selectedcolor,...;
                        'LineStyle',obj.selectedlinesytle,...
                        'LineWidth',obj.selectedlinewidth);
                case false
                    set(obj.handle,'Color',obj.color,...;
                        'LineStyle',obj.linestyle,...
                        'LineWidth',obj.linewidth);
                otherwise
                    error('LineUI selected state must be either true or false');
            end
        end

    end % methods

end % classdef