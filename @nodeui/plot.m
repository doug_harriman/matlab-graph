%PLOT Plot a NodeUI object.
%

% <AUTHOR>
% </AUTHOR>

function [obj] = plot(obj)

% Rectangle
pos = [obj.position(1:2) - [obj.width obj.height]/2 obj.width obj.height];
obj.rect.position = pos;
[han,obj.rect] = plot(obj.rect);
hold('on');

% Label Name
obj.label_name.position = [obj.position 0];
[han,obj.label_name] = plot(obj.label_name);

% Label Handle
if ~isempty(obj.handle)
    obj.label_handle.string = ['(' num2str(obj.handle) ')'];
    obj.label_handle.position = obj.position + [obj.width/2 -obj.height/2];
    [han,obj.label_handle] = plot(obj.label_handle);
end

if nargout < 1
    assignin('caller',inputname(1),obj);
end