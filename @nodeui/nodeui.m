%NODEUI  NodeUI class definition.
%

% <AUTHOR>
% </AUTHOR>

classdef (CaseInsensitiveProperties=true) nodeui < node
%% Public Properties
    properties
        % Basic properties
        position = [0 0];
        width    = 1;
        height   = 1;
        color    = [0 0 0];
        
        % UI Elements
        rect          = rect;
        label_name    = label;
        label_handle  = label;
        
        selected      = false;
        selectedcolor = 'r';
        selectedlinewidth = 2;
        
        % UI Controls
        buttondownfcn  = '';
        uicontextmenu  = [];
        
    end
%% Public Methods
    methods
        %------------------------------------------------------------------
        % NodeUI Constructor
        %------------------------------------------------------------------
        function obj = nodeui(varargin)
            
            % Promote Node objects to NodeUI objects.
            if (nargin > 0) &&  isa(varargin{1},'node')
                % Pull out node
                n        = varargin{1};
                varargin = varargin(2:end);
                
                % Append current graph attributes to list to update.
%                 fields = fieldnames(struct(n));
                fields = properties(n);
                for i = 1:length(fields)
                    obj.(fields{i}) = n.(fields{i});
                end
            end

            % Apply passed in property values.
            if length(varargin) > 1
                % Put the options into a struct.
                S = struct(obj);
                [S,changes,unhandled] = options(S,varargin{:});

                % Apply the changes.
                n_changes = length(changes);
                for i = 1:n_changes
                    change = changes{i};
                    obj.(change) = S.(change);
                end

                % Warn on the unhandled properties.
                n_unhandled = length(unhandled);
                [toolbox_name,toolbox_module] = version(obj);
                for i = 1:2:n_unhandled
                    prop = unhandled{i};
                    warning([toolbox_name ':' toolbox_module ':' 'unknownProperty'],...
                        'Unknown node property (ignored): %s', upper(prop));
                end
            end
            
            % Apply properties to contained objects.
            obj.label_name.string = obj.name;
            obj.rect.curvature    = [1 1];
            obj.label_handle.visible = 'off';

            % Selection color
            obj.rect.selectededgecolor     = obj.selectedcolor;
            obj.label_name.selectedcolor   = obj.selectedcolor;
            obj.label_handle.selectedcolor = obj.selectedcolor;
            obj.rect.selectedlinewidth     = obj.selectedlinewidth;
            
            % Userdata for self referencing.
            data.type                 = 'node';
            data.handle               = obj.handle;
            obj.rect.userdata         = data;
            obj.label_name.userdata   = data;
            obj.label_handle.userdata = data;
        end
        
        %------------------------------------------------------------------
        % Display
        %------------------------------------------------------------------
        % Use parent class display.
        
        %------------------------------------------------------------------
        % Set Color
        %------------------------------------------------------------------
        function obj = set.color(obj,color)
            % Set color of all sub objects before returning.
            obj.color              = color;
            obj.label_handle.color = color;
            obj.label_name.color   = color;
            obj.rect.edgecolor     = color;
            
        end % set.color
        
        %------------------------------------------------------------------
        % Set Selected
        %------------------------------------------------------------------
        function obj = set.selected(obj,value)
            obj.selected              = value;
            obj.rect.selected         = value;
            obj.label_name.selected   = value;
            obj.label_handle.selected = value;
        end

        %------------------------------------------------------------------
        % Set SelectedColor
        %------------------------------------------------------------------
        function obj = set.selectedcolor(obj,value)
            obj.selectededgecolor          = value;
            obj.rect.selectededgecolor     = value;
            obj.label_name.selectedcolor   = value;
            obj.label_handle.selectedcolor = value;
        end

        %------------------------------------------------------------------
        % Set SelectedLineWidth
        %------------------------------------------------------------------
        function obj = set.selectedlinewidth(obj,value)
            obj.selectedlinewidth  = value;
            obj.rect.selectedlinewidth = value;
        end

        %------------------------------------------------------------------
        % Set ButtonDownFcn
        %------------------------------------------------------------------
        function obj = set.buttondownfcn(obj,value)
            obj.rect.buttondownfcn = value;
            obj.label_name.buttondownfcn   = value;
            obj.label_handle.buttondownfcn = value;
        end

        %------------------------------------------------------------------
        % Set UIContextMenu
        %------------------------------------------------------------------
        function obj = set.uicontextmenu(obj,value)
            obj.rect.uicontextmenu = value;
            obj.label_name.uicontextmenu   = value;
            obj.label_handle.uicontextmenu = value;
        end

        %------------------------------------------------------------------
        % Version
        % TODO: make private.
        %------------------------------------------------------------------
        function [toolbox_name,toolbox_module,toolbox_version] = version(obj)
            %<TOOLBOX_INFO>
            TOOLBOX_NAME    = 'GraphToolbox';
            TOOLBOX_MODULE  = 'NodeUI';
            TOOLBOX_VERSION = 1;
            %</TOOLBOX_INFO>

            if nargout == 0
                disp(' ')
                disp(['Toolbox: ' TOOLBOX_NAME]);
                disp(['Version: ' num2str(TOOLBOX_VERSION)]);
                disp(['Module : ' TOOLBOX_MODULE]);
                disp(' ')
                return;
            end

            toolbox_name    = TOOLBOX_NAME;
            toolbox_module  = TOOLBOX_MODULE;
            toolbox_version = TOOLBOX_VERSION;
        end % graph.version
        
        
        
    end % methods
    
end % classdef