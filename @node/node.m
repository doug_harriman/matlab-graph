%NODE  Node class definition.
%

% <AUTHOR>
% </AUTHOR>

classdef (CaseInsensitiveProperties=true) node < handle
%% Public Properties
    properties
        % Basic properties
        name     = '';
        handle   = [];
        userdata = [];
    end
%% Public Methods
    methods
        %------------------------------------------------------------------
        % Node Constructor
        %------------------------------------------------------------------
        function obj = node(varargin)
            if nargin > 0
                % Make sure we have even arg cnt
                if isodd(nargin)
                    error([toolbox_name ':' toolbox_module ':' 'badinput'],...
                        'Property/value pairs expected.');
                end
                
                % Set each property as requested
                props = properties(obj);
                for i = 1:2:nargin
                    % Input pair
                    prop = lower(varargin{i});
                    val  = varargin{i+1};
                    
                    % Check
                    if ~ismember(prop,props)
                        error([toolbox_name ':' toolbox_module ':' 'unknownproperty'],...
                            'Unknown property: %s',prop);
                    end
                    
                    % Set
                    obj.(prop) = val;
                end
            end
            
        end
        
        %------------------------------------------------------------------
        % Display
        %------------------------------------------------------------------
        function display(n)
            n_node = length(n);
            if n_node > 1
                disp(['Node Array (' num2str(n_node) ')']);
            end
            
            for i = 1:n_node
                if isempty(n(i).name)
                    name = '(Unnamed)';
                else
                    name = n(i).name;
                end
                disp(['Node: ' name]);
            end
        end
        
        %------------------------------------------------------------------
        % Version
        % TODO: make private.
        %------------------------------------------------------------------
        function [toolbox_name,toolbox_module,toolbox_version] = version(obj)
            %<TOOLBOX_INFO>
            TOOLBOX_NAME    = 'GraphToolbox';
            TOOLBOX_MODULE  = 'Node';
            TOOLBOX_VERSION = 1;
            %</TOOLBOX_INFO>

            if nargout == 0
                disp(' ')
                disp(['Toolbox: ' TOOLBOX_NAME]);
                disp(['Version: ' num2str(TOOLBOX_VERSION)]);
                disp(['Module : ' TOOLBOX_MODULE]);
                disp(' ')
                return;
            end

            toolbox_name    = TOOLBOX_NAME;
            toolbox_module  = TOOLBOX_MODULE;
            toolbox_version = TOOLBOX_VERSION;
        end % graph.version
    end % methods
    
end % classdef